<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


    Route::get('/', function () { return redirect()->route('login'); })->name('index');
    Route::get('/login','ProcessoSeletivo\LoginController@index')->name('login');
    Route::get('/logout','ProcessoSeletivo\LoginController@sair')->name('logout');
    Route::post('/autenticar','ProcessoSeletivo\LoginController@autenticar')->name('login.autenticar');
    
    Route::group(['middleware' => 'acessoRestritoMiddleware'], function () {        
        Route::get('/home', 'ProcessoSeletivo\LoginController@home')->name('home');

        Route::get('/necessidade-especial', 'ProcessoSeletivo\NecessidadeEspecialController@listar')->name('necessidadeEspecial.listar');
        Route::get('/necessidade-especial/novo', 'ProcessoSeletivo\NecessidadeEspecialController@viewNovo')->name('necessidadeEspecial.novo');
        Route::post('/necessidade-especial/novo/salvar', 'ProcessoSeletivo\NecessidadeEspecialController@novo')->name('necessidadeEspecial.novo-salvar');
        Route::get('/necessidade-especial/alterar/{id}', 'ProcessoSeletivo\NecessidadeEspecialController@viewAlterar')->name('necessidadeEspecial.alterar');
        Route::post('/necessidade-especial/alterar/salvar', 'ProcessoSeletivo\NecessidadeEspecialController@alterar')->name('necessidadeEspecial.alterar-salvar');
        Route::post('/necessidade-especial/remover', 'ProcessoSeletivo\NecessidadeEspecialController@remover')->name('necessidadeEspecial.remover');

        Route::get('/curso', 'ProcessoSeletivo\CursoController@listar')->name('curso.listar');
        Route::get('/curso/novo', 'ProcessoSeletivo\CursoController@viewNovo')->name('curso.novo');
        Route::post('/curso/novo/salvar', 'ProcessoSeletivo\CursoController@novo')->name('curso.novo-salvar');
        Route::get('/curso/alterar/{id}', 'ProcessoSeletivo\CursoController@viewAlterar')->name('curso.alterar');
        Route::post('/curso/alterar/salvar', 'ProcessoSeletivo\CursoController@alterar')->name('curso.alterar-salvar');
        Route::post('/curso/remover', 'ProcessoSeletivo\CursoController@remover')->name('curso.remover');

        Route::get('/questionario-socioeconomico', 'ProcessoSeletivo\QuestionarioSocioeconomicoController@listar')->name('questionarioSocioeconomico.listar');
        Route::get('/questionario-socioeconomico/novo', 'ProcessoSeletivo\QuestionarioSocioeconomicoController@viewNovo')->name('questionarioSocioeconomico.novo');
        Route::post('/questionario-socioeconomico/novo/salvar', 'ProcessoSeletivo\QuestionarioSocioeconomicoController@novo')->name('questionarioSocioeconomico.novo-salvar');
        Route::get('/questionario-socioeconomico/alterar/{id}', 'ProcessoSeletivo\QuestionarioSocioeconomicoController@viewAlterar')->name('questionarioSocioeconomico.alterar');
        Route::post('/questionario-socioeconomico/alterar/salvar', 'ProcessoSeletivo\QuestionarioSocioeconomicoController@alterar')->name('questionarioSocioeconomico.alterar-salvar');
        Route::post('/questionario-socioeconomico/remover', 'ProcessoSeletivo\QuestionarioSocioeconomicoController@remover')->name('questionarioSocioeconomico.remover');

        Route::get('/questionario-socioeconomico/{id}/questao-socioeconomico', 'ProcessoSeletivo\QuestaoSocioeconomicoController@listar')->name('questaoSocioeconomico.listar');
        Route::get('/questionario-socioeconomico/{id}/questao-socioeconomico/novo', 'ProcessoSeletivo\QuestaoSocioeconomicoController@viewNovo')->name('questaoSocioeconomico.novo');
        Route::post('/questionario-socioeconomico/{id}/questao-socioeconomico/novo/salvar', 'ProcessoSeletivo\QuestaoSocioeconomicoController@novo')->name('questaoSocioeconomico.novo-salvar');
        Route::get('/questionario-socioeconomico/{id}/questao-socioeconomico/alterar/{questao}', 'ProcessoSeletivo\QuestaoSocioeconomicoController@viewAlterar')->name('questaoSocioeconomico.alterar');
        Route::post('/questionario-socioeconomico/{id}/questao-socioeconomico/alterar/salvar', 'ProcessoSeletivo\QuestaoSocioeconomicoController@alterar')->name('questaoSocioeconomico.alterar-salvar');
        Route::post('/questionario-socioeconomico/{id}/questao-socioeconomico/remover', 'ProcessoSeletivo\QuestaoSocioeconomicoController@remover')->name('questaoSocioeconomico.remover');

        Route::get('/processo', 'ProcessoSeletivo\ProcessoController@listar')->name('processo.listar');
        Route::get('/processo/novo', 'ProcessoSeletivo\ProcessoController@viewNovo')->name('processo.novo');
        Route::post('/processo/novo/salvar', 'ProcessoSeletivo\ProcessoController@novo')->name('processo.novo-salvar');
        Route::get('/processo/alterar/{id}', 'ProcessoSeletivo\ProcessoController@viewAlterar')->name('processo.alterar');
        Route::post('/processo/alterar/salvar', 'ProcessoSeletivo\ProcessoController@alterar')->name('processo.alterar-salvar');
        Route::post('/processo/remover', 'ProcessoSeletivo\ProcessoController@remover')->name('processo.remover');

        Route::get('/unidade', 'ProcessoSeletivo\UnidadeController@listar')->name('unidade.listar');
        Route::get('/unidade/novo', 'ProcessoSeletivo\UnidadeController@viewNovo')->name('unidade.novo');
        Route::post('/unidade/novo/salvar', 'ProcessoSeletivo\UnidadeController@novo')->name('unidade.novo-salvar');
        Route::get('/unidade/alterar/{id}', 'ProcessoSeletivo\UnidadeController@viewAlterar')->name('unidade.alterar');
        Route::post('/unidade/alterar/salvar', 'ProcessoSeletivo\UnidadeController@alterar')->name('unidade.alterar-salvar');
        Route::post('/unidade/remover', 'ProcessoSeletivo\UnidadeController@remover')->name('unidade.remover');

        Route::get('/vaga', 'ProcessoSeletivo\VagaController@listar')->name('vaga.listar');
        Route::get('/vaga/novo', 'ProcessoSeletivo\VagaController@viewNovo')->name('vaga.novo');
        Route::post('/vaga/novo/salvar', 'ProcessoSeletivo\VagaController@novo')->name('vaga.novo-salvar');
        Route::get('/vaga/alterar/{id}', 'ProcessoSeletivo\VagaController@viewAlterar')->name('vaga.alterar');
        Route::post('/vaga/alterar/salvar', 'ProcessoSeletivo\VagaController@alterar')->name('vaga.alterar-salvar');
        Route::post('/vaga/remover', 'ProcessoSeletivo\VagaController@remover')->name('vaga.remover');

        Route::get('/vaga/{vaga_id}/prova', 'ProcessoSeletivo\ProvaController@listar')->name('prova.listar');
        Route::get('/vaga/{vaga_id}/prova/novo', 'ProcessoSeletivo\ProvaController@viewNovo')->name('prova.novo');
        Route::post('/vaga/{vaga_id}/prova/novo/salvar', 'ProcessoSeletivo\ProvaController@novo')->name('prova.novo-salvar');
        Route::get('/vaga/{vaga_id}/prova/alterar/{id}', 'ProcessoSeletivo\ProvaController@viewAlterar')->name('prova.alterar');
        Route::post('/vaga/{vaga_id}/prova/alterar/salvar', 'ProcessoSeletivo\ProvaController@alterar')->name('prova.alterar-salvar');
        Route::post('/vaga/{vaga_id}/prova/remover', 'ProcessoSeletivo\ProvaController@remover')->name('prova.remover');

        Route::get('/prova/{prova_id}/questao', 'ProcessoSeletivo\QuestaoController@listar')->name('questao.listar');
        Route::get('/prova/{prova_id}/questao/novo', 'ProcessoSeletivo\QuestaoController@viewNovo')->name('questao.novo');
        Route::post('/prova/{prova_id}/questao/novo/salvar', 'ProcessoSeletivo\QuestaoController@novo')->name('questao.novo-salvar');
        Route::get('/prova/{prova_id}/questao/alterar/{id}', 'ProcessoSeletivo\QuestaoController@viewAlterar')->name('questao.alterar');
        Route::post('/prova/{prova_id}/questao/alterar/salvar', 'ProcessoSeletivo\QuestaoController@alterar')->name('questao.alterar-salvar');
        Route::post('/prova/{prova_id}/questao/remover', 'ProcessoSeletivo\QuestaoController@remover')->name('questao.remover');

        Route::get('/importacao', 'ProcessoSeletivo\ImportacaoController@listar')->name('importacao.listar');
        Route::post('/importacao/enviar/arquivo', 'ProcessoSeletivo\ImportacaoController@enviarArquivo')->name('importacao.enviar.arquivo');

        Route::get('/relatorio/situacao','ProcessoSeletivo\VagaController@relatorioSituacao')->name('relatorio.situacao');
    });
# }