$('#novo').on('click', function(){
    window.location.href = $(this).data('link');
});

$('#voltar').on('click', function(){
    window.location.href = $(this).data('link');
});

$('#cancelar').on('click', function(){
    window.location.href = $(this).data('link');
});

$('.alterar').on('click', function(){
    window.location.href = $(this).data('link');
});

$('.remover').on('click', function(){
    var elemento = $(this);
    var linha = $(elemento).parents('tr');
    
    swal({
        title: "Tem certeza que deseja remover este item?",
        text:"Não será possível reverter esta operação.",
        type:"warning",
        buttons: true,
        dangerMode: true,
    })
    .then((verdade) => {
        if(verdade){
            $.ajax({
                type: 'POST',
                url: elemento.data('link'),
                data: { id: elemento.data('id') },
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success:function(retorno){
                    animacao(linha, 'bounceOutRight', 2);
                    swal("Sucesso!", retorno.mensagem, "success");
                },
                error: function(resposta){
                    var erros = '';
                    var retorno = resposta.responseJSON.errors;
                    $.each(retorno, function(){
                        erros = erros+this+"\n";
                    });
                    swal("Erro!", erros, "error"); 
                },
                complete: function(){
                }
            }); 
        }
    },function(t){
    });
});

function animacao(objeto, efeito, acao) { //acao = remover (2) ou adicionar(1)
    objeto.addClass(efeito + " animated").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
        if(acao == 2){
            $(this).remove();
        }          
    })
}

//funcoes genericas
function formatarData(date, formato){
    var data = new Date(date);
    var dia,mes,ano,hora,minuto,segundo,milisegundo;

    if(data.getDate() < 10)
        dia = '0'+data.getDate();
    else
        dia = data.getDate();
    
    if(data.getMonth()+1<10)
        mes = '0'+(data.getMonth()+1);
    else
        mes = data.getMonth()+1;
    
    ano = data.getFullYear();

    if(data.getHours()+1<10)
        hora = '0'+data.getHours();
    else
        hora = data.getHours();

    if(data.getMinutes()<10)
        minuto = '0'+data.getMinutes();
    else
        minuto = data.getMinutes();

    if(data.getSeconds()<10)    
        segundo = '0'+data.getSeconds();
    else
        segundo = data.getSeconds();
    
    if(data.getMilliseconds()<10)
        milisegundo = '00'+data.getMilliseconds();
    else if(data.getMilliseconds()<100)
        milisegundo = '0'+data.getMilliseconds();
    else
        milisegundo = data.getMilliseconds();
    
    //Padrão BR 
    if(formato=='d/m/Y H:m:s.u')
        return dia+'/'+mes+'/'+ano+' '+hora+':'+minuto+':'+segundo+'.'+milisegundo;
    
    if(formato=='d/m/Y H:m:s')
        return dia+'/'+mes+'/'+ano+' '+hora+':'+minuto+':'+segundo;

    if(formato=='d/m/Y H:m')
        return dia+'/'+mes+'/'+ano+' '+hora+':'+minuto;

    if(formato=='d/m/Y H')
        return dia+'/'+mes+'/'+ano+' '+hora;
    
    if(formato=='d/m/Y')
        return dia+'/'+mes+'/'+ano;
    
    //Padrão EN
    if(formato=='m/d/Y H:m:s.u')
        return mes+'/'+dia+'/'+ano+' '+hora+':'+minuto+':'+segundo+'.'+milisegundo;
    
    if(formato=='m/d/Y H:m:s')
        return mes+'/'+dia+'/'+ano+' '+hora+':'+minuto+':'+segundo;

    if(formato=='m/d/Y H:m')
        return mes+'/'+dia+'/'+ano+' '+hora+':'+minuto;

    if(formato=='m/d/Y H')
        return mes+'/'+dia+'/'+ano+' '+hora;
    
    if(formato=='m/d/Y')
        return mes+'/'+dia+'/'+ano;

    if(formato=='m-d-Y H:m:s.u')
        return mes+'-'+dia+'-'+ano+' '+hora+':'+minuto+':'+segundo+'.'+milisegundo;
    
    if(formato=='m-d-Y H:m:s')
        return mes+'-'+dia+'-'+ano+' '+hora+':'+minuto+':'+segundo;

    if(formato=='m-d-Y H:m')
        return mes+'-'+dia+'-'+ano+' '+hora+':'+minuto;

    if(formato=='m-d-Y H')
        return mes+'-'+dia+'-'+ano+' '+hora;
    
    if(formato=='m-d-Y')
        return mes+'-'+dia+'-'+ano;
    
    //padrão Banco de Dados
    if(formato=='Y-m-d H:m:s.u')
        return ano+'-'+mes+'-'+dia+' '+hora+':'+minuto+':'+segundo+'.'+milisegundo;
    
    if(formato=='Y-m-d H:m:s')
        return ano+'-'+mes+'-'+dia+' '+hora+':'+minuto+':'+segundo;

    if(formato=='Y-m-d H:m')
        return ano+'-'+mes+'-'+dia+' '+hora+':'+minuto;

    if(formato=='Y-m-d H')
        return ano+'-'+mes+'-'+dia+' '+hora;
    
    if(formato=='Y-m-d')
        return ano+'-'+mes+'-'+dia;

    return data;    
}

var arrayDeCores = ['#3a87ad', '#9fda96', '#f3bc56', '#43cce7', '#6a7dd3', '#b977d8', '#da969f', '#FFB90F', '#AB82FF', '#FFE1FF', '#BEBEBE', '#87CEFA', '#B0C4DE',
                    '#7FFFD4', '#20B2AA', '#9ACD32', '#BC8F8F', '#FFDEAD', '#7B68EE', '#EE82EE', '#CD5C5C', '#FA8072', '#FF7F50', '#FFA500', '#FFE4C4', '#E6E6FA'];