<?php
namespace App\Http\Middleware;

use App\Exceptions\UsuarioNaoAutenticadoException;
use App\Exceptions\UsuarioBloqueadoException;
use App\Events\PaginaRequisitada;
use App\Modelos\ProcessoSeletivo\Usuario;
use Closure;
use Session;

class AcessoRestritoMiddleware
{
    public function handle($request, Closure $next)
    {    
        throw_unless(Session::has('usuario'), new UsuarioNaoAutenticadoException());   
        event(new PaginaRequisitada($request));     
        $this->consultarBloqueioUsuario($request);
                
        return $next($request);
    }
    
    private function consultarBloqueioUsuario($request){
        $usuario = Usuario::find(Session::get('usuario')->id);
        throw_unless($usuario, new UsuarioBloqueadoException());
        throw_if(!$usuario->bool_ativo, new UsuarioBloqueadoException());
    }
}
