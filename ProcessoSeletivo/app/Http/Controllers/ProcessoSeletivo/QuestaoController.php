<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Modelos\ProcessoSeletivo\Prova;
use App\Modelos\ProcessoSeletivo\Questao;
use App\Modelos\ProcessoSeletivo\Vaga;

class QuestaoController extends Controller
{
    public function listar(int $prova_id, Request $request){
        $prova = Prova::find($prova_id);
        throw_unless($prova, new ErroValidacaoException('A prova informada não foi encontrada.'));

        return view('processo-seletivo.questao.listar')
                ->with('pesquisa', $request->pesquisa)
                ->with('prova', $prova)
                ->with('questoes', $this->buscarPorNome($prova->id, $request->pesquisa));
    }

    private function buscarPorNome(int $prova_id, $query){
        return Questao::where('prova_id', $prova_id)->where('str_pergunta', 'like', '%'.$query.'%')->orderBy('int_ordem')->where('removido_em', null)->paginate(10);
    }

    public function viewNovo(int $prova_id){
        $prova = Prova::find($prova_id);
        throw_unless($prova, new ErroValidacaoException('A prova informada não foi encontrada.'));

        return view('processo-seletivo.questao.novo')
                ->with('prova', $prova);
    }

    public function novo(Request $request){
        $request->validate([
            'str_pergunta' => 'required',
            'str_resposta' => 'required',
            'int_ordem' => 'required',
            'float_valor' => 'required',
            'prova_id' => 'required|exists:provas,id',
        ],[
            'str_pergunta.required' => 'A pergunta deve ser informada.',
            'str_resposta.required' => 'A resposta da pergunta deve ser informada.',
            'int_ordem.required' => 'A ordem deve ser informada.',
            'float_valor.required' => 'O valor deve ser informado.',
            'prova_id.required' => 'A prova deve ser informada.',
            'prova_id.exists' => 'A prova informada não foi encontrada.',
        ]);

        Questao::create([
            'str_pergunta' => Validador::tratarCampo($request->str_pergunta),
            'str_resposta' => $request->str_resposta,
            'int_ordem' => $request->int_ordem,
            'float_valor' => $request->float_valor,
            'prova_id' => $request->prova_id,
        ]);

        $mensagem = "Questão salva com sucesso.";
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function viewAlterar(int $prova_id, $id){
        $prova = Prova::find($prova_id);
        throw_unless($prova, new ErroValidacaoException('Não foi possível alterar. Prova não encontrada.'));

        $questao = Questao::find($id);
        throw_unless($prova, new ErroValidacaoException('Não foi possível alterar. Questão não encontrada.'));

        return view('processo-seletivo.questao.alterar')
                ->with('prova', $prova)
                ->with('questao', $questao);
    }

    public function alterar(int $prova_id, Request $request){
        $request->validate([
            'str_pergunta' => 'required',
            'str_resposta' => 'required',
            'int_ordem' => 'required',
            'float_valor' => 'required',
            'prova_id' => 'required|exists:vagas,id',
        ],[
            'str_pergunta.required' => 'A pergunta deve ser informada.',
            'str_resposta.required' => 'A resposta da pergunta deve ser informada.',
            'int_ordem.required' => 'A ordem deve ser informada.',
            'float_valor.required' => 'O valor deve ser informado.',
            'prova_id.required' => 'A prova deve ser informada.',
            'prova_id.exists' => 'A prova informada não foi encontrada.',
        ]);

        $prova = Prova::find($prova_id);
        throw_unless($prova, new ErroValidacaoException('Não foi possível alterar. Prova não encontrada.'));

        $questao = Questao::find($request->id);
        throw_unless($prova, new ErroValidacaoException('Não foi possível alterar. Questão não encontrada.'));

        $questao->str_pergunta = Validador::tratarCampo($request->str_pergunta);
        $questao->str_resposta = $request->str_resposta;
        $questao->int_ordem = $request->int_ordem;
        $questao->float_valor = $request->float_valor;
        $questao->prova_id = $request->prova_id;
        $questao->save();

        $mensagem = 'Questão salva com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function remover(int $prova_id, Request $request){
        $prova = Prova::find($prova_id);
        throw_unless($prova, new ErroValidacaoException('Não foi possível remover. Prova não encontrada.'));
        $questao = Questao::find($request->id);
        throw_unless($questao, new ErroValidacaoException('Não foi possível remover. Questão não encontrada.'));

        $questao->delete();

        return response()->json(['mensagem' => 'Questão removida com sucesso.'], 200);
    }
}
