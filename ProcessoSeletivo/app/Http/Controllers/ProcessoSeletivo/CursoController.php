<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Modelos\ProcessoSeletivo\Curso;

class CursoController extends Controller
{
    public function listar(Request $request){
        return view('processo-seletivo.curso.listar')
                ->with('pesquisa', $request->pesquisa)
                ->with('cursos', $this->buscarPorNome($request->pesquisa));
    }

    private function buscarPorNome($query){
        return Curso::where('str_nome', 'like', '%'.$query.'%')->where('removido_em', null)->orderby('str_nome')->paginate(10);
    }

    public function viewNovo(){
        return view('processo-seletivo.curso.novo');
    }

    public function novo(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:cursos,str_nome,NULL,id,removido_em,NULL',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
        ]);
        
        Curso::create([
            'str_nome' => Validador::tratarCampo($request->str_nome),
            'str_area_ensino' => Validador::tratarCampo($request->str_area_ensino),
        ]);
        
        $mensagem = "Curso $request->str_nome salvo com sucesso.";
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function viewAlterar($id){
        $curso = Curso::find($id);
        throw_unless($curso, new ErroValidacaoException('Não foi possível remover. Curso não encontrado.'));

        return view('processo-seletivo.curso.alterar')->with('curso', $curso);
    }

    public function alterar(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:cursos,str_nome,'.$request->input('id').',id,removido_em,NULL',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
        ]);

        $curso = Curso::find($request->id);
        throw_unless($curso, new ErroValidacaoException('Não foi possível remover. Curso não encontrado.'));

        $curso->str_nome = Validador::tratarCampo($request->str_nome);
        $curso->str_area_ensino = Validador::tratarCampo($request->str_area_ensino);
        $curso->save();

        $mensagem = 'Curso '.$curso->str_nome.' salva com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function remover(Request $request){
        $curso = Curso::find($request->input('id'));
        throw_unless($curso, new ErroValidacaoException('Não foi possível remover. Curso não encontrada.'));
        
        $curso->delete();
        
        return response()->json(['mensagem' => 'Curso '.$curso->str_nome.' removida com sucesso.'], 200);
    }
}