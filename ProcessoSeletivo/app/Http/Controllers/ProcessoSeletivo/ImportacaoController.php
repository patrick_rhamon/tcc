<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Enums\StatusImportacaoEnum;
use App\Modelos\ProcessoSeletivo\Importacao;
use Illuminate\Support\Facades\Storage;

class ImportacaoController extends Controller
{
    public function listar(Request $request)
    {
        $statuses = [];
        $statuses[StatusImportacaoEnum::AGUARDANDO] = 'Aguardando Processamento';
        $statuses[StatusImportacaoEnum::INDEFIRIDO] = 'Arquivo Indeferido';
        $statuses[StatusImportacaoEnum::PROCESSANDO] = 'Processando Arquivo';
        $statuses[StatusImportacaoEnum::FINALIZADO] = 'Processamento Finalizado';
        return view('processo-seletivo.importacao.listar')
                ->with('pesquisa', $request->pesquisa)
                ->with('statuses', $statuses)
                ->with('importacoes', $this->buscarPorNome($request->pesquisa));
    }

    private function buscarPorNome($query)
    {
        return Importacao::where('str_nome_original', 'like', '%'.$query.'%')
                ->where('removido_em', null)->paginate(10);
    }

    public function enviarArquivo(Request $request)
    {
        return DB::transaction(function() use($request){
            if (!$request->hasFile('arquivo')) {
                throw new ErroValidacaoException('Não foi enviado arquivo!');
            }
    
            $file = $request->file('arquivo');
            $nome_original = $file->getClientOriginalName();
            $nome = date('YmdHis').'.txt';
    
            $caminho = Storage::putFileAs('public/importacao', $request->file('arquivo'), $nome);
    
            $importacao = Importacao::create([
                'str_nome' => $nome,
                'str_nome_original' => $nome_original,
                'int_status' => StatusImportacaoEnum::AGUARDANDO,
                'str_path' => $caminho,
                'data_processamento' => null,    
            ]);
            
            return redirect()->route('importacao.listar');
        });
    }
}