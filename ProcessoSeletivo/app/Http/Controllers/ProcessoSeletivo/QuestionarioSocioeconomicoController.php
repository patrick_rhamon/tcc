<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Modelos\ProcessoSeletivo\QuestionarioSocioeconomico;

class QuestionarioSocioeconomicoController extends Controller
{
    public function listar(Request $request){
        return view('processo-seletivo.questionario-socioeconomico.listar')
                ->with('pesquisa', $request->pesquisa)
                ->with('questionarios_socioeconomicos', $this->buscarPorNome($request->pesquisa));
    }

    private function buscarPorNome($query){
        return QuestionarioSocioeconomico::where('str_nome', 'like', '%'.$query.'%')->where('removido_em', null)->orderby('str_nome')->paginate(10);
    }

    public function viewNovo(){
        return view('processo-seletivo.questionario-socioeconomico.novo');
    }

    public function novo(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:questionarios_socioeconomicos,str_nome,NULL,id,removido_em,NULL',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
        ]);
        
        $questionario_socioeconomico = new QuestionarioSocioeconomico();
        $questionario_socioeconomico->str_nome = Validador::tratarCampo($request->input('str_nome'));
        $questionario_socioeconomico->save();
        
        $mensagem = 'Questionário Socioeconomico '.$questionario_socioeconomico->str_nome.' salvo com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200)->header('Content-Type', 'text/plain');
    }

    public function viewAlterar($id){
        $questionario_socioeconomico = QuestionarioSocioeconomico::find($id);
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Não foi possível remover. Questionário Socioeconomico não encontrado.'));

        return view('processo-seletivo.questionario-socioeconomico.alterar')->with('questionario_socioeconomico', $questionario_socioeconomico);
    }

    public function alterar(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:questionarios_socioeconomicos,str_nome,'.$request->input('id').',id,removido_em,NULL',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
        ]);

        $questionario_socioeconomico = QuestionarioSocioeconomico::find($request->input('id'));
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Não foi possível remover. Questionário Socioeconomico não encontrado.'));

        $questionario_socioeconomico->str_nome = Validador::tratarCampo($request->input('str_nome'));
        $questionario_socioeconomico->save();

        $mensagem = 'Questionário Socioeconomico '.$questionario_socioeconomico->str_nome.' salvo com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200)->header('Content-Type', 'text/plain');
    }

    public function remover(Request $request){
        $questionario_socioeconomico = QuestionarioSocioeconomico::find($request->input('id'));
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Não foi possível remover. Questionário Socioeconomico não encontrado.'));
        
        $questionario_socioeconomico->delete();
        
        return response()->json(['mensagem' => 'Questionário Socioeconomico '.$questionario_socioeconomico->str_nome.' removido com sucesso.'], 200)->header('Content-Type', 'text/plain');
    }
}