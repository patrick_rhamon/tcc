<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Modelos\ProcessoSeletivo\Curso;
use App\Modelos\ProcessoSeletivo\Processo;
use App\Modelos\ProcessoSeletivo\Unidade;
use App\Modelos\ProcessoSeletivo\Vaga;

class VagaController extends Controller
{
    public function listar(Request $request){
        return view('processo-seletivo.vaga.listar')
                ->with('pesquisa', $request->pesquisa)
                ->with('vagas', $this->buscarPorNome($request->pesquisa));
    }

    private function buscarPorNome($query){
        return Vaga::join('cursos', 'cursos.id', 'vagas.curso_id')
                    ->join('processos', 'processos.id', 'vagas.processo_id')
                    ->join('unidades', 'unidades.id', 'vagas.unidade_id')
                    ->where('cursos.str_nome', 'like', '%'.$query.'%')
                    ->orWhere('unidades.str_nome', 'like', '%'.$query.'%')
                    ->orWhere('processos.str_nome', 'like', '%'.$query.'%')
                    ->select('vagas.*')
                    ->where('vagas.removido_em', null)->paginate(10);
    }

    public function viewNovo(){
        return view('processo-seletivo.vaga.novo')  
                ->with('cursos', Curso::all())
                ->with('unidades', Unidade::all())
                ->with('processos', Processo::all());
    }

    public function novo(Request $request){
        $request->validate([
            'processo_id' => 'required|exists:processos,id',
            'curso_id' => 'required|exists:cursos,id',
            'unidade_id' => 'required|exists:unidades,id',
            'int_quantidade' => 'required',
        ],[
            'processo_id.required' => 'O processo seletivo deve ser informado.',
            'processo_id.exists' => 'O processo informado não foi encontrado.',
            'curso_id.required' => 'O curso deve ser informado.',
            'curso_id.exists' => 'O curso informado não foi encontrado.',
            'unidade_id.required' => 'A unidade deve ser informada.', 
            'unidade_id.exists' => 'A unidade informada não foi encontrada.', 
            'int_quantidade.required' => 'A quantidade deve ser informada.',
        ]);
        
        Vaga::create([
            'processo_id' => $request->processo_id,
            'curso_id' => $request->curso_id,
            'unidade_id' => $request->unidade_id,
            'int_quantidade' => $request->int_quantidade,
        ]);
        
        $mensagem = "Vaga salva com sucesso.";
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function viewAlterar($id){
        $vaga = Vaga::find($id);
        throw_unless($vaga, new ErroValidacaoException('Não foi possível alterar. Vaga não encontrada.'));

        return view('processo-seletivo.vaga.alterar')
                ->with('vaga', $vaga)
                ->with('cursos', Curso::all())
                ->with('unidades', Unidade::all())
                ->with('processos', Processo::all());
    }

    public function alterar(Request $request){
        $request->validate([
            'processo_id' => 'required|exists:processos,id',
            'curso_id' => 'required|exists:cursos,id',
            'unidade_id' => 'required|exists:unidades,id',
            'int_quantidade' => 'required',
        ],[
            'processo_id.required' => 'O processo seletivo deve ser informado.',
            'processo_id.exists' => 'O processo informado não foi encontrado.',
            'curso_id.required' => 'O curso deve ser informado.',
            'curso_id.exists' => 'O curso informado não foi encontrado.',
            'unidade_id.required' => 'A unidade deve ser informada.', 
            'unidade_id.exists' => 'A unidade informada não foi encontrada.', 
            'int_quantidade.required' => 'A quantidade deve ser informada.',
        ]);

        $vaga = Vaga::find($request->id);
        throw_unless($vaga, new ErroValidacaoException('Não foi possível alterar. Vaga não encontrada.'));

        $vaga->processo_id = $request->processo_id;
        $vaga->curso_id = $request->curso_id;
        $vaga->unidade_id = $request->unidade_id;
        $vaga->int_quantidade = $request->int_quantidade;
        $vaga->save();

        $mensagem = 'Vaga salva com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function remover(Request $request){
        $vaga = Vaga::find($request->id);
        throw_unless($vaga, new ErroValidacaoException('Não foi possível remover. Vaga não encontrada.'));
        
        $vaga->delete();
        
        return response()->json(['mensagem' => 'Vaga removida com sucesso.'], 200);
    }

    public function relatorioSituacao()
    {
        $vagas = Vaga::all();
        return view('processo-seletivo.relatorio.relatorio', compact(['vagas']));
    }
}