<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Modelos\ProcessoSeletivo\Prova;
use App\Modelos\ProcessoSeletivo\Vaga;

class ProvaController extends Controller
{
    public function listar(int $vaga_id, Request $request){
        $vaga = Vaga::find($vaga_id);
        throw_unless($vaga, new ErroValidacaoException('A vaga informada não foi encontrada.'));
        return view('processo-seletivo.prova.listar')
                ->with('pesquisa', $request->pesquisa)
                ->with('vaga', $vaga)
                ->with('provas', $this->buscarPorNome($vaga->id, $request->pesquisa));
    }

    private function buscarPorNome($vaga_id, $query){
        return Prova::where('vaga_id', $vaga_id)->where('str_nome', 'like', '%'.$query.'%')->orderBy('int_ordem')->where('removido_em', null)->paginate(10);
    }

    public function viewNovo(int $vaga_id){
        $vaga = Vaga::find($vaga_id);
        throw_unless($vaga, new ErroValidacaoException('A vaga informada não foi encontrada.'));
        return view('processo-seletivo.prova.novo')
                ->with('vaga', $vaga);
    }

    public function novo(Request $request){
        $request->validate([
            'str_nome' => 'required',
            'int_quantidade_de_perguntas' => 'required',
            'int_ordem' => 'required',
            'int_peso' => 'required',
            'vaga_id' => 'required|exists:vagas,id',
        ],[
            'str_nome.required' => 'A quantidade deve ser informada.',
            'int_quantidade_de_perguntas.required' => 'A quantidade de perguntas deve ser informada.',
            'int_ordem.required' => 'A ordem deve ser informada.',
            'int_peso.required' => 'O peso deve ser informado.',
            'vaga_id.required' => 'A vaga deve ser informada.',
            'vaga_id.exists' => 'A vaga informada não foi encontrada.',
        ]);

        Prova::create([
            'str_nome' => Validador::tratarCampo($request->str_nome),
            'int_quantidade_de_perguntas' => $request->int_quantidade_de_perguntas,
            'int_ordem' => $request->int_ordem,
            'int_peso' => $request->int_peso,
            'vaga_id' => $request->vaga_id,
        ]);

        $mensagem = "Prova salva com sucesso.";
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function viewAlterar(int $vaga_id, $id){
        $vaga = Vaga::find($vaga_id);
        $prova = Prova::find($id);
        throw_unless($vaga, new ErroValidacaoException('Não foi possível alterar. Vaga não encontrada.'));
        throw_unless($prova, new ErroValidacaoException('Não foi possível alterar. Prova não encontrada.'));

        return view('processo-seletivo.prova.alterar')
                ->with('prova', $prova)
                ->with('vaga', $vaga);
    }

    public function alterar(Request $request){
        $request->validate([
            'str_nome' => 'required',
            'int_quantidade_de_perguntas' => 'required',
            'int_ordem' => 'required',
            'int_peso' => 'required',
            'vaga_id' => 'required|exists:vagas,id',
        ],[
            'str_nome.required' => 'A quantidade deve ser informada.',
            'int_quantidade_de_perguntas.required' => 'A quantidade de perguntas deve ser informada.',
            'int_ordem.required' => 'A ordem deve ser informada.',
            'int_peso.required' => 'O peso deve ser informado.',
            'vaga_id.required' => 'A vaga deve ser informada.',
            'vaga_id.exists' => 'A vaga informada não foi encontrada.',
        ]);

        $prova = Prova::find($request->id);
        throw_unless($prova, new ErroValidacaoException('Não foi possível alterar. Prova não encontrada.'));

        $prova->str_nome = Validador::tratarCampo($request->str_nome);
        $prova->int_quantidade_de_perguntas = $request->int_quantidade_de_perguntas;
        $prova->int_ordem = $request->int_ordem;
        $prova->int_peso = $request->int_peso;
        $prova->vaga_id = $request->vaga_id;
        $prova->save();

        $mensagem = 'Prova salva com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function remover(Request $request){
        $prova = Prova::find($request->id);
        throw_unless($prova, new ErroValidacaoException('Não foi possível remover. Prova não encontrada.'));

        $prova->delete();

        return response()->json(['mensagem' => 'Vaga removida com sucesso.'], 200);
    }
}
