<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Modelos\ProcessoSeletivo\Processo;
use App\Modelos\ProcessoSeletivo\QuestionarioSocioeconomico;

class ProcessoController extends Controller
{
    public function listar(Request $request){
        return view('processo-seletivo.processo.listar')
                ->with('pesquisa', $request->pesquisa)
                ->with('processos', $this->buscarPorNome($request->pesquisa));
    }

    private function buscarPorNome($query){
        return Processo::where('str_nome', 'like', '%'.$query.'%')->where('removido_em', null)->orderby('str_nome')->paginate(20);
    }

    public function viewNovo(){
        return view('processo-seletivo.processo.novo')
                ->with('questionarios', QuestionarioSocioeconomico::all());
    }

    public function novo(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:processos,str_nome,NULL,id,removido_em,NULL',
            'data_inicio' => 'required',
            'data_fim' => 'required',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
            'data_inicio.required' => 'A data inicial deve ser informada.',
            'data_fim.required' => 'A data final deve ser informado.',
        ]);
        
        Processo::create([
            'str_nome' => Validador::tratarCampo($request->str_nome),
            'data_inicio' => Validador::formatarData('d/m/Y', 'Y-m-d', $request->data_inicio),
            'data_fim' => Validador::formatarData('d/m/Y', 'Y-m-d', $request->data_fim),
            'data_recebimento' => Validador::formatarData('d/m/Y', 'Y-m-d', $request->data_recebimento),
            'questionario_socioeconomico_id' => $request->questionario_socioeconomico_id,
            'data_prova' => Validador::formatarData('d/m/Y', 'Y-m-d', $request->data_prova),
            'str_turno' => Validador::tratarCampo($request->str_turno),
            'str_tipo' => Validador::tratarCampo($request->str_tipo),
            'float_valor' => Validador::validarMonetario($request->float_valor),
            'bool_situacao' => false,
        ]);
                
        $mensagem = 'Processo '.$request->str_nome.' salvo com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function viewAlterar($id){
        $processo = Processo::find($id);
        throw_unless($processo, new ErroValidacaoException('Não foi possível alterar. Processo Seletivo não encontrado.'));

        return view('processo-seletivo.processo.alterar')
                ->with('processo', $processo)
                ->with('questionarios', QuestionarioSocioeconomico::all());
    }

    public function alterar(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:cursos,str_nome,'.$request->input('id').',id,removido_em,NULL',
            'data_inicio' => 'required',
            'data_fim' => 'required',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
            'data_inicio.required' => 'A data inicial deve ser informada.',
            'data_fim.required' => 'A data final deve ser informado.',
        ]);

        $processo = Processo::find($request->id);
        throw_unless($processo, new ErroValidacaoException('Não foi possível alterar. Processo Seletivo não encontrado.'));

        $processo->update([
            'str_nome' => Validador::tratarCampo($request->input('str_nome')),
            'data_inicio' => Validador::formatarData('d/m/Y', 'Y-m-d H:i:s.v', $request->input('data_inicio')),
            'data_fim' => Validador::formatarData('d/m/Y', 'Y-m-d H:i:s.v', $request->input('data_fim')),
            'data_recebimento' => Validador::formatarData('d/m/Y', 'Y-m-d H:i:s.v', $request->input('data_recebimento')),
            'questionario_socioeconomico_id' => $request->input('questionario_socioeconomico_id'),
            'data_prova' => Validador::formatarData('d/m/Y', 'Y-m-d H:i:s.v', $request->input('data_prova')),
            'str_turno' => Validador::tratarCampo($request->input('str_turno')),
            'str_tipo' => Validador::tratarCampo($request->input('str_tipo')),
            'float_valor' => Validador::validarMonetario($request->input('float_valor')),
        ]);

        $mensagem = 'Processo '.$processo->str_nome.' salvo com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function remover(Request $request){
        $processo = Processo::find($request->input('id'));
        throw_unless($processo, new ErroValidacaoException('Não foi possível remover. Processo não encontrada.'));
        
        $processo->delete();
        
        return response()->json(['mensagem' => 'Processo '.$processo->str_nome.' removido com sucesso.'], 200);
    }
}