<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Modelos\ProcessoSeletivo\Unidade;
use App\Modelos\ProcessoSeletivo\Endereco;

class UnidadeController extends Controller
{
    public function listar(Request $request){
        return view('processo-seletivo.unidade.listar')
                ->with('pesquisa', $request->pesquisa)
                ->with('unidades', $this->buscarPorNome($request->pesquisa));
    }

    private function buscarPorNome($query){
        return Unidade::where('str_nome', 'like', '%'.$query.'%')->where('removido_em', null)->orderby('str_nome')->paginate(10);
    }

    public function viewNovo(){
        return view('processo-seletivo.unidade.novo');
    }

    public function novo(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:unidades,str_nome,NULL,id,removido_em,NULL',
            'str_email' => 'email',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
            'str_email.email' => 'O e-mail informado é invalido.',
        ]);

        $endereco = null;
        $endereco_id = null;

        if($request->str_cep){
            $endereco = Endereco::create([
                'str_cep' => $request->str_cep,
                'str_logradouro' => $request->str_logradouro,
                'str_bairro' => $request->str_bairro,
                'str_cidade' => $request->str_cidade,
                'str_estado' => $request->str_estado,
                'str_numero' => $request->str_numero,
                'str_complemento' => $request->str_complemento,
            ]);
        }

        if($endereco) $endereco_id = $endereco->id;

        Unidade::create([
            'str_nome' => Validador::tratarCampo($request->input('str_nome')),
            'str_telefone' => $request->input('str_telefone'),
            'str_celular' => $request->input('str_celular'),
            'str_email' => $request->input('str_email'),
            'endereco_id' => $endereco_id,
        ]);
        
        $mensagem = "Unidade $request->str_nome salva com sucesso.";
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function viewAlterar($id){
        $unidade = Unidade::find($id);
        throw_unless($unidade, new ErroValidacaoException('Unidade não encontrada.'));

        return view('processo-seletivo.unidade.alterar')->with('unidade', $unidade);
    }

    public function alterar(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:unidades,str_nome,'.$request->id.',id,removido_em,NULL',
            'str_email' => 'email',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
            'str_email.email' => 'O e-mail informado é invalido.',
        ]);

        $unidade = Unidade::find($request->id);
        throw_unless($unidade, new ErroValidacaoException('Unidade não encontrada.'));
        
        if($unidade->endereco){
            if($request->str_cep){
                $unidade->endereco->alterar($request->str_cep, $request->str_logradouro, $request->str_bairro, $request->str_cidade, $request->str_estado, $request->str_numero, $request->str_complemento);
                $unidade->endereco->save();
            }else{
                $unidade->endereco->delete();
            }
        }else{
            if($request->str_cep){
                $endereco = Endereco::create([
                    'str_cep' => $request->str_cep,
                    'str_logradouro' => $request->str_logradouro,
                    'str_bairro' => $request->str_bairro,
                    'str_cidade' => $request->str_cidade,
                    'str_estado' => $request->str_estado,
                    'str_numero' => $request->str_numero,
                    'str_complemento' => $request->str_complemento,
                ]);
                $unidade->endereco_id = $endereco->id;
            }
        }

        $unidade->alterar($request->str_nome, $request->str_telefone, $request->str_celular, $request->str_email);
        $unidade->save();

        $mensagem = "Unidade $unidade->str_nome alterada com sucesso!";
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function remover(Request $request){
        $unidade = Unidade::find($request->id);
        throw_unless($unidade, new ErroValidacaoException('Unidade não encontrada.'));

        if($unidade->endereco) $unidade->endereco->delete();
        
        $unidade->delete();
        
        return response()->json(['mensagem' => "Unidade $unidade->str_nome foi removida com sucesso!"], 200);
    }
}