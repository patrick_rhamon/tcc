<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Modelos\ProcessoSeletivo\NecessidadeEspecial;

class NecessidadeEspecialController extends Controller
{
    public function listar(Request $request){
        return view('processo-seletivo.necessidade-especial.listar')
                ->with('pesquisa', $request->input('str_pesquisa'))
                ->with('necessidades_especiais', $this->buscarPorNome($request->input('str_pesquisa')));
    }

    private function buscarPorNome($query){
        return NecessidadeEspecial::where('str_nome', 'like', '%'.$query.'%')->where('removido_em', null)->orderby('str_nome')->paginate(20);
    }

    public function viewNovo(){
        return view('processo-seletivo.necessidade-especial.novo');
    }

    public function novo(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:necessidades_especiais,str_nome,NULL,id,removido_em,NULL',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
        ]);
        
        $necessidade_especial = new NecessidadeEspecial();
        $necessidade_especial->str_nome = Validador::tratarCampo($request->input('str_nome'));
        $necessidade_especial->bool_acessibilidade = $request->input('bool_acessibilidade') ? 1 : 0;
        $necessidade_especial->save();
        
        $mensagem = 'Necessidade Especial '.$necessidade_especial->str_nome.' salva com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function viewAlterar($id){
        $necessidade_especial = NecessidadeEspecial::find($id);
        throw_unless($necessidade_especial, new ErroValidacaoException('Não foi possível remover. Necessidade Especial não encontrada.'));

        return view('processo-seletivo.necessidade-especial.alterar')->with('necessidade_especial', $necessidade_especial);
    }

    public function alterar(Request $request){
        $request->validate([
            'str_nome' => 'required|unique:necessidades_especiais,str_nome,'.$request->input('id').',id,removido_em,NULL',
        ],[
            'str_nome.required' => 'O nome deve ser informado.',
            'str_nome.unique' => 'O nome informado já se encontra cadastrado.',
        ]);

        $necessidade_especial = NecessidadeEspecial::find($request->input('id'));
        throw_unless($necessidade_especial, new ErroValidacaoException('Não foi possível remover. Necessidade Especial não encontrada.'));

        $necessidade_especial->str_nome = Validador::tratarCampo($request->input('str_nome'));
        $necessidade_especial->bool_acessibilidade = $request->input('bool_acessibilidade') ? 1 : 0;
        $necessidade_especial->save();

        $mensagem = 'Necessidade Especial '.$necessidade_especial->str_nome.' salva com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200);
    }

    public function remover(Request $request){
        $necessidade_especial = NecessidadeEspecial::find($request->input('id'));
        throw_unless($necessidade_especial, new ErroValidacaoException('Não foi possível remover. Necessidade Especial não encontrada.'));
        throw_if($necessidade_especial->verificarSeExistemReferencias(), new ErroValidacaoException('Não foi possível remover. Necessidade especial vinculada a outros registros.'));
        $necessidade_especial->delete();
        
        return response()->json(['mensagem' => 'Necessidade Especial '.$necessidade_especial->str_nome.' removida com sucesso.'], 200);
    }
}