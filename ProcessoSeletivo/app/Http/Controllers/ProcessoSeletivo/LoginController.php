<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Exceptions\ErroValidacaoException;
use Validator;
use Session;
use App\Modelos\ProcessoSeletivo\Usuario;
use App\Extras\Validador;

class LoginController extends Controller
{
    public function index(){
        if(Session::get('usuario'))
            return redirect()->route('home');
        return view('index');
    }

    public function home(){
        return view('processo-seletivo.home.home');
    }

    public function autenticar(Request $request){
    	$this->validatorAutenticarUsuario($request);
                 
        $usuario = $this->buscarUsuario($request);
        Session::put('usuario', $usuario);

        return redirect()->route('home');
    }

    private function buscarUsuario($request){
            $str_login = $request->input('str_login');        
            $str_senha = $request->input('str_senha');
            
            if($this->verificarSeLoginEhCpf($str_login))
                $str_login = Validador::tratarCpf($str_login, 14);

            $usuario = Usuario::join('pessoas', 'pessoas.id', 'usuarios.pessoa_id')
                                ->where(function($query) use($str_login){
                                    $query->where('str_login', $str_login);
                                    $query->whereNotNull('str_login');
                                })
                                ->orWhere(function($query) use($str_login){
                                    $query->where('pessoas.str_email', $str_login);
                                    $query->whereNotNull('pessoas.str_email');
                                })
                                ->orWhere(function($query) use($str_login){
                                    $query->where('pessoas.str_cpf', $str_login);
                                    $query->whereNotNull('pessoas.str_cpf');
                                })
                                ->select('usuarios.*')
                                ->first();
            
            throw_unless($usuario, new ErroValidacaoException('Usuário/senha inválidos.'));
            throw_unless(Hash::check($str_senha, $usuario->str_senha), new ErroValidacaoException('Usuário/senha inválidos.'));
            
            return $usuario;
    }

    private function verificarSeLoginEhCpf($str_login){
        if((strlen($str_login) == 11) && (preg_match('/[0-9]/', $str_login)))
            return true;
    }

    public function sair(){
        Session::flush();
        return redirect()->route('login');
    }

    private function validatorAutenticarUsuario(Request $request){
        $request->validate([
                    'str_login' => 'required',
                    'str_senha' => 'required',        
                 ],            
                 [
                    'str_login.required' => 'Informe o Login',
                    'str_senha.required' => 'Informe a Senha',
                 ]
        );               
    }
}