<?php

namespace App\Http\Controllers\ProcessoSeletivo;

use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Exceptions\ErroValidacaoException;
use App\Extras\Validador;
use App\Extras\TipoPerguntaEnum;
use App\Modelos\ProcessoSeletivo\QuestionarioSocioeconomico;
use App\Modelos\ProcessoSeletivo\QuestaoSocioeconomico;

class QuestaoSocioeconomicoController extends Controller
{
    public function filtros(){
        $filtros = array();
        array_push($filtros, ['id' => TipoPerguntaEnum::DESCRITIVA, 'descricao' => 'Descritiva']);
        array_push($filtros, ['id' => TipoPerguntaEnum::MULTIPLA_ESCOLHA, 'descricao' => 'Múltipla Escolha']);
        array_push($filtros, ['id' => TipoPerguntaEnum::SIM_NAO, 'descricao' => 'Sim / Não']);
        
        return $filtros;
    }

    public function listar($id, Request $request){
        $questionario_socioeconomico = QuestionarioSocioeconomico::find($id);
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Questionário Socioeconomico não encontrado.'));

        $questoes_socioeconomicos = $this->buscarPorNome($request->input('str_pesquisa'), $questionario_socioeconomico);

        return view('processo-seletivo.questao-socioeconomico.listar')
                ->with('pesquisa', $request->input('str_pesquisa'))
                ->with('questionario_socioeconomico', $questionario_socioeconomico)
                ->with('questoes_socioeconomicos', $questoes_socioeconomicos);
    }

    private function buscarPorNome($query, $questionario_socioeconomico){
        return QuestaoSocioeconomico::where('str_pergunta', 'like', '%'.$query.'%')
                                    ->where('questionario_socioeconomico_id', $questionario_socioeconomico->id)
                                    ->where('removido_em', null)->orderby('str_pergunta')->paginate(20);
    }

    public function viewNovo($id){
        $questionario_socioeconomico = QuestionarioSocioeconomico::find($id);
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Questionário Socioeconomico não encontrado.'));

        return view('processo-seletivo.questao-socioeconomico.novo')
                ->with('questionario_socioeconomico', $questionario_socioeconomico)
                ->with('filtros', $this->filtros());
    }

    public function novo(Request $request){
        $request->validate([
            'str_pergunta' => 'required|unique:questoes_socioeconomicos,str_pergunta,NULL,id,removido_em,NULL',
            'int_tipo_resposta' => 'required',
        ],[
            'str_pergunta.required' => 'A pergunta deve ser informada.',
            'str_pergunta.unique' => 'A pergunta informada já se encontra cadastrada.',
            'int_tipo_resposta.required' => 'O tipo da pergunta deve ser informado.',
        ]);
        
        $questionario_socioeconomico = QuestionarioSocioeconomico::find($request->input('questionario_id'));
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Questionário Socioeconomico não encontrado.'));

        QuestaoSocioeconomico::create([
            'str_pergunta' => Validador::tratarCampo($request->input('str_pergunta')),
            'int_tipo_resposta' => $request->input('int_tipo_resposta'),
            'questionario_socioeconomico_id' => $questionario_socioeconomico->id,
        ]);

        $mensagem = 'Questão '.$request->input('str_pergunta').' salva com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200)->header('Content-Type', 'text/plain');
    }

    public function viewAlterar($id, $questao){
        $questionario_socioeconomico = QuestionarioSocioeconomico::find($id);
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Não foi possível alterar. Questionário Socioeconomico não encontrado.'));

        $questao_socioeconomico = QuestaoSocioeconomico::find($questao);
        throw_unless($questao_socioeconomico, new ErroValidacaoException('Não foi possível alterar. Questão Socioeconomico não encontrada.'));

        return view('processo-seletivo.questao-socioeconomico.alterar')
                ->with('questionario_socioeconomico', $questionario_socioeconomico)
                ->with('questao_socioeconomico', $questao_socioeconomico)
                ->with('filtros', $this->filtros());
    }

    public function alterar(Request $request){
        $request->validate([
            'str_pergunta' => 'required|unique:questoes_socioeconomicos,str_pergunta,'.$request->input('id').',id,removido_em,NULL',
            'int_tipo_resposta' => 'required',
        ],[
            'str_pergunta.required' => 'A pergunta deve ser informada.',
            'str_pergunta.unique' => 'A pergunta informada já se encontra cadastrada.',
            'int_tipo_resposta.required' => 'O tipo da pergunta deve ser informado.',
        ]);

        $questionario_socioeconomico = QuestionarioSocioeconomico::find($request->input('questionario_id'));
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Não foi possível alterar. Questionário Socioeconomico não encontrado.'));

        $questao_socioeconomico = QuestaoSocioeconomico::find($request->input('id'));
        throw_unless($questao_socioeconomico, new ErroValidacaoException('Não foi possível alterar. Questão Socioeconomico não encontrada.'));

        $questao_socioeconomico->update([
            'str_pergunta' => Validador::tratarCampo($request->input('str_pergunta')),
            'int_tipo_resposta' => $request->input('int_tipo_resposta'),
            'questionario_socioeconomico_id' => $questionario_socioeconomico->id,
        ]);

        $mensagem = 'Questão Socioeconomico '.$questao_socioeconomico->str_pergunta.' salva com sucesso.';
        return response()->json(['mensagem' => $mensagem], 200)->header('Content-Type', 'text/plain');
    }

    public function remover($id, Request $request){
        $questionario_socioeconomico = QuestionarioSocioeconomico::find($id);
        throw_unless($questionario_socioeconomico, new ErroValidacaoException('Não foi possível remover. Questionário Socioeconomico não encontrado.'));
        
        $questao_socioeconomico = QuestaoSocioeconomico::find($request->input('id'));
        throw_unless($questao_socioeconomico, new ErroValidacaoException('Não foi possível remover. Questão Socioeconomico não encontrada.'));

        $questao_socioeconomico->delete();
        
        return response()->json(['mensagem' => 'Questão Socioeconomico '.$questao_socioeconomico->str_pergunta.' removida com sucesso.'], 200)->header('Content-Type', 'text/plain');
    }
}