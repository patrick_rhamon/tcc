<?php
namespace App\Extras;
use App\Exceptions\ErroValidacaoException;

class Validador
{
    public static function tratarCpf($cpf, $tamanhoSaida, $levantarExcecao = true){
        $cpf = trim($cpf);
        throw_if(strlen($cpf) != 11 && strlen($cpf) != 14 && $levantarExcecao, new ErroValidacaoException('Formato inválido do CPF')); 

        if($tamanhoSaida == 14){
            $cpf = substr_replace($cpf, '.', 3, 0);
            $cpf = substr_replace($cpf, '.', 7, 0);
            $cpf = substr_replace($cpf, '-', 11, 0);
            return $cpf;
        }

        if($tamanhoSaida == 11){
            $cpf = preg_replace( '/[^0-9]/', '', $cpf);
            return $cpf;
        }        
    }

    public static function gerarStringAleatoria($tamanho, $caracteres){
        $quantidadeCaracteres = strlen($caracteres); 
        $quantidadeCaracteres--; 

        $string=NULL; 
        for($x = 1; $x <= $tamanho; $x++){ 
            $posicao = rand(0,$quantidadeCaracteres); 
            $string .= substr($caracteres,$posicao,1); 
        } 

        return $string; 
    } 

    public static function tratarCampo($campo){
        $campo = trim($campo);
        $campo = mb_strtoupper($campo);
        return $campo;
    }

    public static function formatarData($formato_entrada, $formato_saida, $data){
        $dateInfo = date_parse_from_format($formato_entrada, $data);
        $unixTimestamp = mktime($dateInfo['hour'], $dateInfo['minute'], $dateInfo['second'],
            $dateInfo['month'], $dateInfo['day'], $dateInfo['year']);
        
        if($data)
            return date($formato_saida, $unixTimestamp);
        else
            return null;
    }

    public static function verificarSeFormatoDeDataEhValido($sigla_pais, $data){
        switch($sigla_pais){
            case 'BR':
                $data_temporaria = explode('/', $data);
                // checkdate(mes, dia, ano)
                throw_if(count($data_temporaria) < 3, new ErroValidacaoException('Data '.$data. ' inválida.'));
                throw_unless(checkdate($data_temporaria[1], $data_temporaria[0], $data_temporaria[2]), new ErroValidacaoException('Data '.$data. ' inválida.'));
                break;
            default:
                throw new ErroValidacaoException('Informe uma sigla de pais válido para verificar a data');
        }
    }

    public static function validarMonetario($valor){
        if(is_null($valor)) return $valor;
        $valor = str_replace('R', '', $valor);
        $valor = str_replace('$', '', $valor);
        $valor = str_replace(' ', '', $valor);
        $valor = str_replace(',', '.', $valor);
        return $valor;
    }

    public static function validarTelefone($telefone, $digitos, $aceita_nulo = false){
        $telefone = preg_replace( '/[^0-9]/', '', $telefone);
        if(!$aceita_nulo)
            throw_if(strlen($telefone) == 0, new ErroValidacaoException('Informe um número de telefone válido'));
        throw_if(strlen($telefone) > 0 && strlen($telefone) < $digitos, new ErroValidacaoException('Informe um número de telefone válido'));
    }
}