<?php
namespace App\Extras\Enums;

class TipoPerguntaEnum
{
    const DESCRITIVA = 1;
    const MULTIPLA_ESCOLHA = 2;
    const SIM_NAO = 3;
}