<?php
namespace App\Extras\Enums;

class StatusInscricaoEnum
{
    const APROVADO = 1;
    const REPROVADO = 2;
    const REGULAR = 3;
    const DEFERIDO = 4;
    const INDEFERIDO = 5;
}