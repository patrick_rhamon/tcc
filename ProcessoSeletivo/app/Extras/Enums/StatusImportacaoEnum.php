<?php
namespace App\Extras\Enums;

class StatusImportacaoEnum
{
    const AGUARDANDO = 1;
    const INDEFIRIDO = 2;
    const PROCESSANDO = 3;
    const FINALIZADO = 4;
}