<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inscricao extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='inscricoes';
    
    protected $fillable=[
        'float_nota_enem',
        'bool_usa_nota_enem',
        'data_enem',
        'bool_isento',
        'float_nota_processo',
        'usuario_id',
        'status_id',
        'sala_id',
        'vaga_id',
    ];

    public function usuario()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Usuario', 'usuario_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Status', 'status_id');
    }
    
    public function sala()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Sala', 'sala_id');
    }

    public function vaga()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Vaga', 'vaga_id');
    }

    public function respostas()
    {
        return $this->hasMany('App\Modelos\ProcessoSeletivo\RespostaInscricao', 'inscricao_id');
    }
}
