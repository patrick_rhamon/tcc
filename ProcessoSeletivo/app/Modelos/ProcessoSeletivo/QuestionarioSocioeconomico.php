<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionarioSocioeconomico extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='questionarios_socioeconomicos';
    
    protected $fillable=[
         'str_nome'
    ];

    public function processos(){
        return $this->hasMany('App\Modelos\ProcessoSeletivo\Processo', 'questionario_socioeconomico_id');
    }

    public function questoesSocioeconomicos(){
        return $this->hasMany('App\Modelos\ProcessoSeletivo\QuestaoSocioeconomico', 'questionario_socioeconomico_id');
    }

    public function verificarSeExistemReferencias(){
        if($this->processos()->count()) return true;
        if($this->questoesSocioeconomicas()->count()) return true;
    }

}
