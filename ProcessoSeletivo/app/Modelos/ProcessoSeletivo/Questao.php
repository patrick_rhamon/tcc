<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questao extends Model
{
    use SoftDeletes;

    protected $dates = ['removido_em'];

    const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='questoes';

    protected $fillable=[
        'str_pergunta',
        'str_resposta',
        'int_ordem',
        'float_valor',
        'prova_id'
    ];

    public function prova()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Prova', 'prova_id', 'id');
    }
}
