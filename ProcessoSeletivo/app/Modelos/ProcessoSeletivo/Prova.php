<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prova extends Model
{
    use SoftDeletes;

    protected $dates = ['removido_em'];

    const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='provas';

    protected $fillable=[
        'str_nome',
        'int_quantidade_de_perguntas',
        'int_ordem',
        'int_peso',
        'vaga_id'
    ];

    public function vaga()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Vaga', 'vaga_id', 'id');
    }

    public function questoes()
    {
        return $this->hasMany('App\Modelos\ProcessoSeletivo\Questao', 'prova_id');
    }
}
