<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vaga extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='vagas';
    
    protected $fillable=[
        'int_quantidade',
        'processo_id',
        'curso_id',
        'unidade_id'
    ];

    public function processo()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Processo', 'processo_id', 'id');
    }

    public function curso()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Curso', 'curso_id', 'id');
    }

    public function unidade()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Unidade', 'unidade_id', 'id');
    }

    public function provas()
    {
        return $this->hasMany('App\Modelos\ProcessoSeletivo\Prova', 'vaga_id');
    }

    public function inscricoes()
    {
        return $this->hasMany('App\Modelos\ProcessoSeletivo\Inscricao', 'vaga_id');
    }
}
