<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NecessidadeEspecial extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='necessidades_especiais';
    
    protected $fillable=[
         'str_nome',
         'bool_acessibilidade'
    ];

    public function pessoas(){
        return $this->belongsToMany('App\Modelos\ProcessoSeletivo\Pessoa', 'pessoas_necessidades_especiais');
    }

    public function verificarSeExistemReferencias(){
        if($this->pessoas()->count()) return true;
    }

}
