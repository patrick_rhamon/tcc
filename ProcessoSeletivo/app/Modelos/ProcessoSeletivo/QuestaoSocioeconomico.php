<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestaoSocioeconomico extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='questoes_socioeconomicos';
    
    protected $fillable=[
         'str_pergunta',
         'int_tipo_resposta',
         'questionario_socioeconomico_id'
    ];

    public function respostasSocioeconomicos(){
        return $this->hasMany('App\Modelos\ProcessoSeletivo\Processo', 'questao_socioeconomico_id');
    }

    public function questionarioSocioeconomico(){
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\QuestionarioSocioeconomico', 'questionario_socioeconomico_id');
    }

    public function verificarSeExistemReferencias(){
        if($this->respostasSocioeconomicos()->count()) return true;
        if($this->questionarioSocioeconomico()->count()) return true;
    }

}
