<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Endereco extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='enderecos';
    
    protected $fillable=[
         'str_cep',
         'str_logradouro',
         'str_bairro',
         'str_cidade',
         'str_estado',
         'str_numero',
         'str_complemento',
    ];

    public function alterar(string $str_cep, string $str_logradouro, string $str_bairro, string $str_cidade, string $str_estado, string $str_numero, ?string $str_complemento) : void {
        $this->str_cep = $str_cep;
        $this->str_logradouro = $str_logradouro;
        $this->str_bairro = $str_bairro;
        $this->str_cidade = $str_cidade;
        $this->str_estado = $str_estado;
        $this->str_numero = $str_numero;
        $this->str_complemento = $str_complemento;
    }
}
