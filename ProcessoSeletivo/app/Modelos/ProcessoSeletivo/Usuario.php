<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class Usuario extends Model
{
    use SoftDeletes;
    
    protected $dates = ['removido_em'];
    
    const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table = 'usuarios';

    protected $fillable = [
       'str_login',
       'str_senha',
       'bool_ativo',
    ];
    protected $hidden = [
        'str_senha'
    ];

    public static function resolveId(){
        return Session::has('usuario') ? Session::get('usuario')->id : null;
    }

    public function pessoa(){
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Pessoa');
    }

    public function inscricoes()
    {
        return $this->hasMany('App\Modelos\ProcessoSeletivo\Inscricao', 'usuario_id');
    }
}
