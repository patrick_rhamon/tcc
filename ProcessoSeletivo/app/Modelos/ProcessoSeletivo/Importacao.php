<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Importacao extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='importacoes';
    
    protected $fillable=[
        'str_nome',
        'str_nome_original',
        'int_status',
        'str_path',
        'data_processamento',
    ];
}
