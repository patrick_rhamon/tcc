<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sala extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='salas';
    
    protected $fillable=[
        'str_nome',
        'int_quantidade_de_pessoas',
        'int_andar',
        'unidade_id',
    ];
}
