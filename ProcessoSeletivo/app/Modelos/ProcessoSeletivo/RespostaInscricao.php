<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RespostaInscricao extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='respostas_inscricoes';
    
    protected $fillable=[
        'str_resposta',
        'int_acerto',
        'questao_id',
        'inscricao_id',
    ];

    public function questao()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Questao', 'questao_id');
    }

    public function inscricao()
    {
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Inscricao', 'inscricao_id');
    }
}
