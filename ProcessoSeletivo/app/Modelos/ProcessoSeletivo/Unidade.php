<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidade extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='unidades';
    
    protected $fillable=[
         'str_nome',
         'str_telefone',
         'str_celular',
         'str_email',
         'endereco_id',
    ];

    public function alterar(string $str_nome, ?string $str_telefone, ?string $str_celular, string $str_email) : void {
        $this->str_nome = $str_nome;
        $this->str_telefone = $str_telefone;
        $this->str_celular = $str_celular;
        $this->str_email = $str_email;
    }

    public function endereco(){
        return $this->belongsTo('App\Modelos\ProcessoSeletivo\Endereco', 'endereco_id');
    }
}
