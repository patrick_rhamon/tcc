<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Processo extends Model
{
    use SoftDeletes;    
    
    protected $dates = ['removido_em'];
    
	const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table='processos';
    
    protected $fillable=[
        'str_nome',
        'data_inicio',
        'data_fim',
        'data_recebimento',
        'float_valor',
        'bool_situacao',
        'data_prova',
        'str_turno',
        'str_tipo',
        'questionario_socioeconomico_id',
    ];

}
