<?php

namespace App\Modelos\ProcessoSeletivo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Pessoa extends Model
{
  	use SoftDeletes;    
    
    protected $dates = ['removido_em'];

    const CREATED_AT = 'criado_em';
    const UPDATED_AT = 'atualizado_em';
    const DELETED_AT = 'removido_em';

    protected $table = 'pessoas';

    protected $fillable=[
        'str_nome',
        'str_email',
        'str_cpf',
        'str_rg',
        'str_orgao_emissor',
        'str_telefone',
        'str_celular',
        'str_sexo',
        'data_nascimento'
    ];

    public function usuario(){
        return $this->hasOne('App\Modelos\ProcessoSeletivo\Usuario', 'pessoa_id', 'id');
    }

    public function endereco(){
        return $this->hasOne('App\Modelos\ProcessoSeletivo\Endereco', 'pessoa_id', 'id');
    }
}
