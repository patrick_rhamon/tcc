<?php

namespace App\Console\Commands;

use App\Events\Gerenciador\EnviarEmailEvent;
use Illuminate\Console\Command;
use App\Extras\Enums\SistemaGerenciador\StatusEnvioMensagemEnum;
use App\Extras\Enums\SistemaGerenciador\TipoDeEnvioDeMensagem;
use App\Extras\Enums\StatusImportacaoEnum;
use App\Extras\Enums\StatusInscricaoEnum;
use App\Modelos\ProcessoSeletivo\Importacao;
use App\Modelos\ProcessoSeletivo\Inscricao;
use App\Modelos\ProcessoSeletivo\RespostaInscricao;
use App\Repositorios\Gerenciador\Interfaces\IClienteRepositorio;
use App\Repositorios\Gerenciador\Interfaces\IControleEnvioMensagemRepositorio;
use App\Repositorios\Gerenciador\Interfaces\IMensagemEmailRepositorio;
use App\Repositorios\Gerenciador\Interfaces\IModuloRepositorio;
use App\Repositorios\Gerenciador\Interfaces\IPessoaRepositorio;
use Illuminate\Support\Facades\Storage;

class ProcessarImportacao extends Command
{
    protected $signature = 'ProcessarImportacao:cron';
    protected $description = 'Importação dos Arquivos Respostas';

    public function __construct(
    )
    {
        parent::__construct();
    }

    public function handle()
    {
        $importacao = Importacao::where('int_status', StatusImportacaoEnum::AGUARDANDO)->first();

        if($importacao){
            $path = Storage::path($importacao->str_path);
            $arquivo = fopen($path, "r");

            $importacao->update([
                'int_status' => StatusImportacaoEnum::PROCESSANDO,
                'data_processamento' => date('Y-m-d H:i:s'),
            ]);

            while (!feof($arquivo)) {
                $explode = explode(',', fgets($arquivo));
                $inscricao = Inscricao::find($explode[0]);
                
                if ($inscricao) {
                    $vaga = $inscricao->vaga;
                    $provas = $vaga->provas()->orderBy('int_ordem', 'asc')->get();
                    
                    $acertos = 0;
                    $i = 1;
                    foreach ($provas as $prova) {    
                        $questoes = $prova->questoes()->orderBy('int_ordem', 'asc')->get();

                        foreach ($questoes as $questao) {
                            $resposta = RespostaInscricao::where('inscricao_id', $inscricao->id)->where('questao_id', $questao->id)->first();
                            
                            $acerto = $questao->str_resposta == $explode[$i] ? 0 : 1;

                            if ($resposta) {
                                $resposta->update([
                                    'str_resposta' => $explode[$i],
                                    'int_acerto' => $acerto,
                                ]);
                            }else{
                                $resposta = RespostaInscricao::create([
                                    'str_resposta' => $explode[$i],
                                    'int_acerto' => $acerto,
                                    'questao_id' => $questao->id,
                                    'inscricao_id' => $inscricao->id,
                                ]);
                            }                        
                            
                            $acertos = $acertos + $acerto;
                            $i++;
                        }
                    }

                    $porcentagem_acertos = $acertos * 100 / $inscricao->respostas()->count();

                    $inscricao->update([
                        'float_nota_processo' => $acertos,
                        'status_id' => $porcentagem_acertos > 70 ? StatusInscricaoEnum::APROVADO : StatusInscricaoEnum::REPROVADO,
                    ]);
                }
            }
            
            $importacao->update([
                'int_status' => StatusImportacaoEnum::FINALIZADO,
            ]);

            fclose($arquivo);
        }
    }
}
