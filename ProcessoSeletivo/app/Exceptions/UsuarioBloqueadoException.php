<?php

namespace App\Exceptions;
use App\Exceptions\ExceptionPersonalizada;

use Exception;
use Session; 

class UsuarioBloqueadoException extends ExceptionPersonalizada
{
    public function __construct($mensagem = null) {
        parent::__construct($mensagem, null, null);
    }

    public function render($request)
    {
        return view('index');
    }
}