<?php

namespace App\Exceptions;
use App\Exceptions\ExceptionPersonalizada;

use Exception;
use Session; 

class UsuarioNaoAutenticadoException extends ExceptionPersonalizada
{
    public function __construct($mensagem = null) {
        parent::__construct($mensagem, null, null);
    }

    public function render($request){
        $pagina = $request->path().'?'.$request->getQueryString();
        
        Session::put('pagina_acessada', $pagina);
        Session::put('rota_acessada', $request->route()->getName());

        Session::flash('mensagem', 'Faça seu log-in para continuar.');
        Session::flash('status','danger');

        return redirect()->route('login');
    }
}