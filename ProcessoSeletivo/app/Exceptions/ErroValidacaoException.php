<?php

namespace App\Exceptions;
use App\Exceptions\ExceptionPersonalizada;

use Exception;
use Session;

class ErroValidacaoException extends ExceptionPersonalizada
{
    private $retorno;
    private $parametros;

    public function __construct($mensagem = null, $retorno = null, $parametros = null) {
        parent::__construct($mensagem, null, null);
        $this->retorno = $retorno;
        $this->parametros = $parametros;
    }

    public function render($request)
    {
        if($request->ajax()) {
            return response()->json(['mensagem' => $this->getMessage()], 422)->header('Content-Type', 'text/plain');
        }

        if($this->getMessage()){
            Session::flash('mensagem', $this->getMessage());
            Session::flash('status','danger');
        }

        if($this->retorno)
            return redirect()->route($this->retorno, $this->parametros);
        return redirect()->back();
    }
}