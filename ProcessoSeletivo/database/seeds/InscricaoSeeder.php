<?php

use App\Extras\Enums\StatusInscricaoEnum;
use Illuminate\Database\Seeder;
use App\Modelos\ProcessoSeletivo\Endereco;
use App\Modelos\ProcessoSeletivo\Inscricao;
use App\Modelos\ProcessoSeletivo\Pessoa;
use App\Modelos\ProcessoSeletivo\Processo;
use App\Modelos\ProcessoSeletivo\Sala;
use App\Modelos\ProcessoSeletivo\Usuario;
use App\Modelos\ProcessoSeletivo\Vaga;

class InscricaoSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $vaga1 = Vaga::find(1);
        $vaga2 = Vaga::find(3);
        $salas = Sala::all();

        foreach ($salas as $sala) {
            $usuarios = Usuario::join('pessoas', 'pessoas.id', 'usuarios.pessoa_id')
                            ->leftJoin('inscricoes', 'usuarios.id', 'inscricoes.usuario_id')
                            ->whereNull('inscricoes.id')
                            ->where('str_tipo_usuario', 'U')
                            ->select('usuarios.*', 'pessoas.str_nome')->orderBy('pessoas.str_nome')
                            ->take($sala->int_quantidade_de_pessoas)->get();

            foreach ($usuarios as $index => $usuario) {
                $vaga_id = $index%2==0 ? $vaga1->id : $vaga2->id;

                Inscricao::create([
                    'float_nota_enem' => rand(22200, 44400) / 10,
                    'bool_usa_nota_enem' => (bool)random_int(0, 1),
                    'data_enem' => '2020-11-23',
                    'bool_isento' => true,
                    'float_nota_processo' => null,
                    'usuario_id' => $usuario->id,
                    'status_id' => StatusInscricaoEnum::REGULAR,
                    'sala_id' => $sala->id,
                    'vaga_id' => $vaga_id,
                ]);
            }
        }

        $inscricoes = Inscricao::all();
        $respostas = "respostas.txt";
        $arquivo = fopen($respostas, 'w');
        foreach ($inscricoes as $inscricao) {
            fwrite($arquivo,"$inscricao->id,");
            
            for ($i=0; $i<30; $i++) {
                if ($i == 29)
                    fwrite($arquivo,random_int(0, 5));
                else
                    fwrite($arquivo,random_int(0, 5).",");
            }

            fwrite($arquivo,"\n");
        }
        $nome_arquivo = 'respostas_'.date('YmdHis').'.txt';
        Storage::putFileAs('public/importacao', $respostas, $nome_arquivo);
        fclose($arquivo);
    }
}
