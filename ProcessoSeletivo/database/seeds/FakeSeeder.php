<?php

use Illuminate\Database\Seeder;
use App\Modelos\ProcessoSeletivo\Endereco;
use App\Modelos\ProcessoSeletivo\Pessoa;
use App\Modelos\ProcessoSeletivo\Usuario;

class FakeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*
        for ($i=0; $i<500; $i++) {
            $faker = Faker\Factory::create('pt_BR');
            $cpf = $faker->cpf(false);
            $endereco = Endereco::create([       
                'str_logradouro' => $faker->streetName,
                'str_numero' => $faker->buildingNumber,
                'str_bairro' => $faker->cityPrefix.' '.$faker->citySuffix,   
                'str_cidade' => $faker->city,     
                'str_estado' => $faker->stateAbbr,
                'str_complemento' => $faker->secondaryAddress,
                'str_cep' => $faker->postcode,
            ]);

            $pessoa = Pessoa::create([
                'str_nome' => $faker->name,
                'str_cpf' => $cpf,
                'data_nascimento' => $faker->dateTime(),
                'str_email' => $faker->email,
                'str_rg' => $faker->rg(false),
                'str_telefone' => $faker->landlineNumber(false),
                'str_celular' => $faker->phoneNumberCleared,
                'str_sexo' => $i%2==0 ? 'M' : 'F',
                'endereco_id' => $endereco->id,
            ]);

            Usuario::create([       
                'str_login' => $cpf,
                'str_senha' => bcrypt($cpf),
                'str_tipo_usuario' => 'U',
                'bool_ativo' => 1,
                'pessoa_id' => $pessoa->id,
            ]);
        }
        */
    }
}
