<?php

use App\Extras\Enums\StatusInscricaoEnum;
use Illuminate\Database\Seeder;
use App\Modelos\ProcessoSeletivo\Endereco;
use App\Modelos\ProcessoSeletivo\Pessoa;
use App\Modelos\ProcessoSeletivo\Status;
use App\Modelos\ProcessoSeletivo\Usuario;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(Endereco::where('str_logradouro', 'AVENIDA DÉLIO SILVA BRITTO')->where('str_cep', '29105265')->count() == 0){
            $endereco = Endereco::create([       
                'str_logradouro' => 'AVENIDA DÉLIO SILVA BRITTO',
                'str_numero' => '49',
                'str_bairro' => 'SANTA MÔNICA',   
                'str_cidade' => 'VILA VELHA',     
                'str_estado' => 'ES',
                'str_complemento' => 'ESQUINA COM A RUA 10',
                'str_cep' => '29105-265',
            ]);
        }

        if(Pessoa::where('str_cpf', '12527358713')->count() == 0){
            $pessoa = Pessoa::create([
                'str_nome' => 'ADMIN MASTER',
                'str_cpf' => '12527358713',
                'data_nascimento' => '1992-01-29 00:00:00.000',
                'str_email' => 'PRR.MIRANDA@GMAIL.COM',
                'str_rg' => '2.237.812 - ES',
                'str_telefone' => '2730332770',
                'str_celular' => '27992841559',
                'str_sexo' => 'M',
                'endereco_id' => $endereco->id,
            ]);
        }

        if(Usuario::where('str_login', 'admin')->count() == 0){
            $usuario = Usuario::create([       
                'str_login' => 'admin',
                'str_senha' => bcrypt('123456'),
                'str_tipo_usuario' => 'SU',
                'bool_ativo' => 1,
                'pessoa_id' => $pessoa->id,
            ]);
        }

        $data = date('Y-m-d H:i:s');

        Status::insert([
            [
                'id' => StatusInscricaoEnum::APROVADO,
                'str_nome' => 'Aprovado',
                'criado_em' => $data,
                'atualizado_em' => $data,
            ],[
                'id' => StatusInscricaoEnum::REPROVADO,
                'str_nome' => 'Reprovado',
                'criado_em' => $data,
                'atualizado_em' => $data,
            ],[
                'id' => StatusInscricaoEnum::REGULAR,
                'str_nome' => 'Regular',
                'criado_em' => $data,
                'atualizado_em' => $data,
            ],[
                'id' => StatusInscricaoEnum::DEFERIDO,
                'str_nome' => 'Deferido',
                'criado_em' => $data,
                'atualizado_em' => $data,
            ],[
                'id' => StatusInscricaoEnum::INDEFERIDO,
                'str_nome' => 'Indeferido',
                'criado_em' => $data,
                'atualizado_em' => $data,
            ]
        ]);
    }
}
