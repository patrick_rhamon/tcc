<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('enderecos')) {
            Schema::create('enderecos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('str_logradouro')->nullable();
                $table->string('str_numero')->nullable();
                $table->string('str_bairro')->nullable();
                $table->string('str_cidade')->nullable();
                $table->string('str_estado')->nullable();
                $table->string('str_complemento')->nullable();
                $table->string('str_cep')->nullable();
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('necessidades_especiais')) {
            Schema::create('necessidades_especiais', function (Blueprint $table) {
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->integer('bool_acessibilidade')->nullable();
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('pessoas')) {
            Schema::create('pessoas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->string('str_cpf')->nullable();
                $table->dateTime('data_nascimento')->nullable();
                $table->string('str_email')->nullable();
                $table->string('str_rg')->nullable();
                $table->string('str_telefone')->nullable();
                $table->string('str_celular')->nullable();
                $table->string('str_sexo')->nullable();
                $table->integer('endereco_id')->unsigned()->nullable();
                $table->foreign('endereco_id')->references('id')->on('enderecos');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('pessoas_necessidades_especiais')){
            Schema::create('pessoas_necessidades_especiais', function(Blueprint $table){
                $table->integer('pessoa_id')->unsigned();
                $table->foreign('pessoa_id')->references('id')->on('pessoas');
                $table->integer('necessidade_especial_id')->unsigned();
                $table->foreign('necessidade_especial_id')->references('id')->on('necessidades_especiais');
                $table->primary(['pessoa_id', 'necessidade_especial_id'], 'pk_pessoa_necessidade_especial');
            });
        }

        if(!Schema::hasTable('usuarios')){
            Schema::create('usuarios', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_login')->nullable();
                $table->string('str_senha')->nullable();
                $table->string('str_tipo_usuario', 2)->nullable();
                $table->integer('bool_ativo')->nullable();
                $table->integer('pessoa_id')->unsigned()->nullable();
                $table->foreign('pessoa_id')->references('id')->on('pessoas');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('status')){
            Schema::create('status', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('cursos')){
            Schema::create('cursos', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->string('str_area_ensino')->nullable();
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('unidades')){
            Schema::create('unidades', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->string('str_telefone')->nullable();
                $table->string('str_celular')->nullable();
                $table->string('str_email')->nullable();
                $table->integer('endereco_id')->unsigned()->nullable();
                $table->foreign('endereco_id')->references('id')->on('enderecos');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();

            });
        }

        if(!Schema::hasTable('salas')){
            Schema::create('salas', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->integer('int_quantidade_de_pessoas')->nullable();
                $table->integer('int_andar')->nullable();
                $table->integer('unidade_id')->unsigned()->nullable();
                $table->foreign('unidade_id')->references('id')->on('unidades');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();

            });
        }

        if(!Schema::hasTable('questionarios_socioeconomicos')){
            Schema::create('questionarios_socioeconomicos', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();

            });
        }

        if(!Schema::hasTable('questoes_socioeconomicos')){
            Schema::create('questoes_socioeconomicos', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_pergunta')->nullable();
                $table->integer('int_tipo_de_resposta')->nullable();
                $table->integer('questionario_socioeconomico_id')->unsigned()->nullable();
                $table->foreign('questionario_socioeconomico_id')->references('id')->on('questionarios_socioeconomicos');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();

            });
        }

        if(!Schema::hasTable('processos')){
            Schema::create('processos', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->dateTime('data_inicio')->nullable();
                $table->dateTime('data_fim')->nullable();
                $table->dateTime('data_recebimento')->nullable();
                $table->dateTime('data_prova')->nullable();
                $table->float('float_valor', 8, 2)->nullable();
                $table->boolean('bool_situacao')->nullable();
                $table->string('str_turno')->nullable();
                $table->string('str_tipo')->nullable();
                $table->integer('questionario_socioeconomico_id')->unsigned()->nullable();
                $table->foreign('questionario_socioeconomico_id')->references('id')->on('questionarios_socioeconomicos');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('vagas')){
            Schema::create('vagas', function(Blueprint $table){
                $table->increments('id');
                $table->integer('int_quantidade')->nullable();
                $table->integer('processo_id')->unsigned()->nullable();
                $table->foreign('processo_id')->references('id')->on('processos');
                $table->integer('curso_id')->unsigned()->nullable();
                $table->foreign('curso_id')->references('id')->on('cursos');
                $table->integer('unidade_id')->unsigned()->nullable();
                $table->foreign('unidade_id')->references('id')->on('unidades');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('inscricoes')){
            Schema::create('inscricoes', function(Blueprint $table){
                $table->increments('id');
                $table->float('float_nota_enem', 8, 2)->nullable();
                $table->integer('bool_usa_nota_enem')->nullable();
                $table->datetime('data_enem')->nullable();
                $table->integer('bool_isento')->nullable();
                $table->float('float_nota_processo', 8, 2)->nullable();
                $table->integer('usuario_id')->unsigned()->nullable();
                $table->foreign('usuario_id')->references('id')->on('usuarios');
                $table->integer('status_id')->unsigned()->nullable();
                $table->foreign('status_id')->references('id')->on('status');
                $table->integer('sala_id')->unsigned()->nullable();
                $table->foreign('sala_id')->references('id')->on('salas');
                $table->integer('vaga_id')->unsigned()->nullable();
                $table->foreign('vaga_id')->references('id')->on('vagas');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();

            });
        }

        if(!Schema::hasTable('respostas_socioeconomicos')){
            Schema::create('respostas_socioeconomicos', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_resposta')->nullable();
                $table->integer('questao_socioeconomico_id')->unsigned()->nullable();
                $table->foreign('questao_socioeconomico_id')->references('id')->on('questoes_socioeconomicos');
                $table->integer('inscricao_id')->unsigned()->nullable();
                $table->foreign('inscricao_id')->references('id')->on('inscricoes');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();

            });
        }

        if(!Schema::hasTable('provas')){
            Schema::create('provas', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->integer('int_quantidade_de_perguntas')->nullable();
                $table->integer('int_ordem')->nullable();
                $table->integer('int_peso')->nullable();
                $table->integer('vaga_id')->unsigned()->nullable();
                $table->foreign('vaga_id')->references('id')->on('vagas');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('questoes')){
            Schema::create('questoes', function(Blueprint $table){
                $table->increments('id');

                $table->string('str_pergunta')->nullable();
                $table->string('str_resposta')->nullable();
                $table->integer('int_ordem')->nullable();
                $table->float('float_valor')->nullable();
                $table->integer('prova_id')->unsigned()->nullable();
                $table->foreign('prova_id')->references('id')->on('provas');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('respostas_inscricoes')){
            Schema::create('respostas_inscricoes', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_resposta')->nullable();
                $table->integer('int_acerto')->nullable();
                $table->integer('questao_id')->unsigned()->nullable();
                $table->foreign('questao_id')->references('id')->on('questoes');
                $table->integer('inscricao_id')->unsigned()->nullable();
                $table->foreign('inscricao_id')->references('id')->on('inscricoes');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }

        if(!Schema::hasTable('importacoes')){
            Schema::create('importacoes', function(Blueprint $table){
                $table->increments('id');
                $table->string('str_nome')->nullable();
                $table->string('str_nome_original')->nullable();
                $table->integer('int_status')->nullable();
                $table->string('str_path')->nullable();
                $table->dateTime('data_processamento');
                $table->dateTime('criado_em');
                $table->dateTime('atualizado_em');
                $table->dateTime('removido_em')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('importacoes');
        Schema::drop('respostas_inscricoes');
        Schema::drop('questoes');
        Schema::drop('provas');
        Schema::drop('inscricoes');
        Schema::drop('vagas');
        Schema::drop('processos');
        Schema::drop('respostas_socioeconomicos');
        Schema::drop('questoes_socioeconomicos');
        Schema::drop('questionarios_socioeconomicos');
        Schema::drop('salas');
        Schema::drop('unidades');
        Schema::drop('cursos');
        Schema::drop('status');
        Schema::drop('usuarios');
        Schema::drop('pessoas_necessidades_especiais');
        Schema::drop('pessoas');
        Schema::drop('necessidades_especiais');
        Schema::drop('enderecos');
    }
}
