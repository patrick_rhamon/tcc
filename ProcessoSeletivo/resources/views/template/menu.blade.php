<div class="logo">
    <a href="{{ route('home') }}">
        <img src="{{ asset('images/logo.png') }}" alt="Processo Seletivo" />
    </a>
</div>
<div class="menu-sidebar2__content js-scrollbar1">
    <div class="account2">
        <div class="image img-cir img-120">
            <img src="{{ asset('images/profile/padrao.png') }}" alt="{{ Session::get('usuario')->str_nome }}" />
        </div>
        <h4 class="name">{{ Session::get('usuario')->str_nome }}</h4>
        <a href="{{ route('logout') }}">Logout</a>
    </div>
    <nav class="navbar-sidebar2">
        @if(Session::get('usuario')->str_tipo_usuario == 'SU')
            <ul class="list-unstyled navbar__list">
                <li class="active has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-book"></i>Cadastro</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li><a href="{{ route('curso.listar')}}"> <i class="ICONE MENU"></i>Curso</a></li>
                        <li><a href="{{ route('unidade.listar')}}"> <i class="ICONE MENU"></i>Unidade</a></li>
                        <li><a href="{{ route('processo.listar')}}"> <i class="ICONE MENU"></i>Processo Seletivo</a></li>
                        <li><a href="{{ route('vaga.listar')}}"> <i class="ICONE MENU"></i>Vaga</a></li>
                        <!--<li><a href="{{ route('questionarioSocioeconomico.listar')}}"> <i class="ICONE MENU"></i>Questionário Socio Econômico</a></li>-->
                        <!--<li><a href="{{ route('necessidadeEspecial.listar')}}"> <i class="ICONE MENU"></i>Necessidade Especial</a></li>-->
                    </ul>
                </li>
                <li class="active has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-book"></i>Apuração</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li><a href="{{ route('importacao.listar')}}"> <i class="ICONE MENU"></i>Importar Respostas</a></li>
                        <li><a href="{{ route('relatorio.situacao')}}"> <i class="ICONE MENU"></i>Relatório de Situação</a></li>
                    </ul>
                </li>
            </ul>
        @endif
    </nav>
</div>