<!DOCTYPE html>
<html lang="pt_br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Processo Seletivo">
        <meta name="keywords" content="Processo Seletivo">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        
        <title>{{ isset($titulo_pagina) ? $titulo_pagina : '' }}</title>
        {{-- <link href="{{ asset('css/custom.min.css')  }}" rel="stylesheet"> --}}
        
        <!-- Fontfaces CSS-->
        <link href="{{ asset('css/font-face.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="{{ asset('vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/wow/animate.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/slick/slick.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">
        
        <link href="{{ asset('vendor/vector-map/jqvmap.min.css" rel="stylesheet') }}" media="all">

        <!-- Main CSS-->
        <link href="{{ asset('css/theme.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('css/ladda.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" media="all">

        <style>
            .page-wrapper {    
                height: 100vh;
            }
        </style>
        @yield('css') 
    </head>

    <body class="animsition">
        <div class="page-wrapper">
            <aside class="menu-sidebar2">
                	@include('template.menu')
            </aside>
            <div class="page-container2">
                <header class="header-desktop2">
                    @include('template.topbar')
                </header>
                <aside class="menu-sidebar2 js-right-sidebar d-block d-lg-none">
                	@include('template.topbar-mobile')
                </aside>
                @yield('conteudo')
            </div>
        </div>
    </body>
</html>

<!-- Jquery JS-->
<script src="{{ asset('vendor/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('vendor/jquery.mask.min.js') }}"></script>
<script src="{{ asset('vendor/jquery.maskMoney.min.js') }}"></script>
<!-- Bootstrap JS-->
<script src="{{ asset('vendor/bootstrap-4.1/popper.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
<!-- Vendor JS       -->
<script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
<script src="{{ asset('vendor/wow/wow.min.js') }}"></script>
<script src="{{ asset('vendor/animsition/animsition.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
<script src="{{ asset('vendor/counter-up/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('vendor/counter-up/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('vendor/circle-progress/circle-progress.min.js') }}"></script>
<script src="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('vendor/chartjs/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
<script src="{{ asset('vendor/sweetalert/sweet.min.js') }}"></script>

<script src="{{ asset('vendor/vector-map/jquery.vmap.js') }}"></script>
<script src="{{ asset('vendor/vector-map/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('vendor/vector-map/jquery.vmap.sampledata.js') }}"></script>
<script src="{{ asset('vendor/vector-map/jquery.vmap.world.js') }}"></script>

<!-- Main JS-->
<script src="{{ asset('js/main.js') }}"></script>

<script src="{{ asset('js/ladda.min.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/spin.min.js') }}"></script>

<script>
    $(document).ready(function ($) {
        $(".data").mask("99/99/9999");
        $(".monetario").maskMoney({
            prefix: "R$ ",
            decimal: ",",
            thousands: "."
        });
        $('input.numerico').keyup(function(e) {
            if (/\D/g.test(this.value)){
                this.value = this.value.replace(/\D/g, '');
            }
        });
    });
    /*
    $('div.conteudo').css('min-height', (screen.height - $('header.header-desktop2').height())+'px');
    if(screen.height == 1080)
        $('div.conteudo').css('min-height', '816px');

    $('header.header-desktop2').css('max-width', (screen.width - $('aside.menu-sidebar2').width())+'px')
    */
</script>

@yield('scripts') 










