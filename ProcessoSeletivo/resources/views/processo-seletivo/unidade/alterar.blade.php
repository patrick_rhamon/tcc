@extends('template.template', ['titulo' => 'Nova Unidade', 'titulo_pagina' => 'Processo Seletivo'])

@section('css')
@endsection

@section('conteudo')
	<div class="conteudo">
		<section class="au-breadcrumb m-t-75">
			<div class="section__content section__content--p30">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="au-breadcrumb-content">
								<div class="au-breadcrumb-left">
									<span class="au-breadcrumb-span"></span>
									<ul class="list-unstyled list-inline au-breadcrumb__list">
										<li class="list-inline-item active">
											<a href="{{ route('unidade.listar') }}">Unidades</a>
										</li>
										<li class="list-inline-item seprate">
											<span>/</span>
										</li>
										<li class="list-inline-item">Alterar Unidade</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="m-t-15">
			<div class="section__content section__content--p30">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<div class="card">
								<form action="{{ route('unidade.alterar-salvar') }}" method="post" id="form" class="form-horizontal">
									<input type="hidden" name="id" id="id" value="{{ $unidade->id }}">
									<div class="card-header">
										<strong>Alterar Unidade</strong>
									</div>
									<div class="card-body card-block">
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_nome" class=" form-control-label">Nome*</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_nome" required name="str_nome" placeholder="Nome da Unidade" class="form-control" value="{{ $unidade->str_nome }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_email" class=" form-control-label">E-mail*</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="email" id="str_email" required name="str_email" placeholder="E-mail da Unidade" class="form-control" value="{{ $unidade->str_email }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_telefone" class=" form-control-label">Telefone</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_telefone" name="str_telefone" placeholder="Telefone da Unidade" class="form-control" value="{{ $unidade->str_telefone }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_celular" class=" form-control-label">Celular</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_celular" name="str_celular" placeholder="Celular da Unidade" class="form-control" value="{{ $unidade->str_celular }}">
											</div>
										</div>
									</div>
									<hr><div class="card-title">
										<h5> &nbsp; &nbsp; &nbsp; &nbsp; Endereço da Unidade</h5>
									</div><hr>								
									<div class="card-body card-block">
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_cep" class=" form-control-label">CEP</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_cep" name="str_cep" placeholder="CEP do Endereço" class="form-control" value="{{ $unidade->endereco->str_cep ?? '' }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3 m-t-3">
												<label for="str_logradouro" class=" form-control-label">Logradouro</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_logradouro" name="str_logradouro" placeholder="Logradouro do Endereço" class="form-control" value="{{ $unidade->endereco->str_logradouro ?? '' }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_numero" class=" form-control-label">Número</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_numero" name="str_numero" placeholder="Número do Endereço" class="form-control" value="{{ $unidade->endereco->str_numero ?? '' }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_bairro" class=" form-control-label">Bairro</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_bairro" name="str_bairro" placeholder="Bairro do Endereço" class="form-control" value="{{ $unidade->endereco->str_bairro ?? '' }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_cidade" class=" form-control-label">Cidade</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_cidade" name="str_cidade" placeholder="Cidade do Endereço" class="form-control" value="{{ $unidade->endereco->str_cidade ?? '' }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_estado" class=" form-control-label">Estado</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_estado" name="str_estado" placeholder="Estado do Endereço" class="form-control" value="{{ $unidade->endereco->str_estado ?? '' }}">
											</div>
										</div>
										<div class="row form-group">
											<div class="col col-md-3">
												<label for="str_complemento" class=" form-control-label">Complemento</label>
											</div>
											<div class="col-12 col-md-9">
												<input type="text" id="str_complemento" name="str_complemento" placeholder="Complemento do Endereço" class="form-control" value="{{ $unidade->endereco->str_complemento ?? '' }}">
											</div>
										</div>
									</div>
									<div class="card-footer text-center">
										<button type="button" class="btn btn-danger btn-sm" id="cancelar" data-link="{{ route('unidade.listar') }}">
											<i class="fa fa-ban"></i> Cancelar
										</button>
										<button type="button" class="btn btn-primary ladda-button btn-sm" id="salvar" data-retorno="{{ route('unidade.listar') }}">
											<i class="fa fa-save"></i> Salvar
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection

@section('scripts')
	<script>
		$('#salvar').on('click', function(e){
			e.preventDefault();
			//var btn_salvar = Ladda.create(this);
			//btn_salvar.start();

            if(!$("#form").parsley().validate()){
                //btn_salvar.stop();
                return;
            }
        
            var form = $('#form');

            $.ajax({
                method: $(form).attr("method"),
                url: $(form).attr("action"),
                data: $(form).serializeArray(),            
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (retorno) {
					swal("Sucesso!", retorno.mensagem, "success");
                    setTimeout(function() { window.document.location = $('#salvar').data('retorno'); }, 1500);
                },
                error: function (resposta) { 
					var erros = '';
					var retorno = resposta.responseJSON.errors;
					$.each(retorno, function(){
						erros = erros+this+"\n";
					});
                    swal("Erro!", erros, "error");
                },
                complete: function(){
                    //btn_salvar.stop();                
                }
            });
		});
	</script>
@endsection