
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @foreach ($vagas as $vaga)
        <table class="table table-data2">
            <thead>
                <tr>
                    <th colspan="3">Situação da Vaga: {{ $vaga->unidade->str_nome }} - {{ $vaga->curso->str_nome }}</th>
                </tr>
                <tr>
                    <th style="width: 40%;">Nome</th>
                    <th style="width: 30%;" class="text-center">Nota</th>
                    <th style="width: 30%;">Situação</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($vaga->inscricoes()->orderBy('float_nota_processo', 'desc')->orderBy('id', 'asc')->get() as $index => $inscricao)
                    <tr class="tr-shadow">
                        <td>{{ $inscricao->usuario->pessoa->str_nome }}</td>
                        <td>{{ $inscricao->float_nota_processo }}</td>
                        <td>{{ $inscricao->status_id == 2 ? 'Reprovado' : ($index < $vaga->int_quantidade ? 'Aprovado' : 'Suplente') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endforeach
</body>
</html>

