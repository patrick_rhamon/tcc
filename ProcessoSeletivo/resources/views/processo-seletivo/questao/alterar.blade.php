@extends('template.template', ['titulo' => 'Alterar Prova', 'titulo_pagina' => 'Processo Seletivo'])

@section('css')
@endsection

@section('conteudo')
    <div class="conteudo">
        <section class="au-breadcrumb m-t-75">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span"></span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="{{ route('questao.listar', [$prova->id]) }}">Questões</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Alterar Questão</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="m-t-15">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <form action="{{ route('questao.alterar-salvar', [$prova->id]) }}" method="post" id="form" class="form-horizontal">
                                    <input type="hidden" name="id" id="id" value="{{ $questao->id }}">
                                    <div class="card-header">
                                        <strong>Alterar Questão</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="prova_id" class=" form-control-label">Prova*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="hidden" class="form-control" required id="prova_id" name="prova_id" value="{{ $prova->id }}">
                                                <input type="text" class="form-control" disabled value="{{ $prova->str_nome }}">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="str_pergunta" class=" form-control-label">Pergunta*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control" required id="str_pergunta" name="str_pergunta" value="{{ $questao->str_pergunta }}">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="str_resposta" class=" form-control-label">Resposta*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control" required id="str_resposta" name="str_resposta" value="{{ $questao->str_resposta }}">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="int_ordem" class=" form-control-label">Ordem*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control numerico" required id="int_ordem" name="int_ordem" value="{{ $questao->int_ordem }}">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="float_valor" class=" form-control-label">Valor*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control numerico" required id="float_valor" name="float_valor" value="{{ $questao->float_valor }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        <button type="button" class="btn btn-danger btn-sm" id="cancelar" data-link="{{ route('questao.listar', [$prova->id]) }}">
                                            <i class="fa fa-ban"></i> Cancelar
                                        </button>
                                        <button type="button" class="btn btn-primary ladda-button btn-sm" id="salvar" data-retorno="{{ route('questao.listar', [$prova->id]) }}">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
	<script>
		$('#salvar').on('click', function(e){
			e.preventDefault();
			//var btn_salvar = Ladda.create(this);
			//btn_salvar.start();

            if(!$("#form").parsley().validate()){
                //btn_salvar.stop();
                return;
            }
        
            var form = $('#form');

            $.ajax({
                method: $(form).attr("method"),
                url: $(form).attr("action"),
                data: $(form).serializeArray(),
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (retorno) {
					swal("Sucesso!", retorno.mensagem, "success");
                    setTimeout(function() { window.document.location = $('#salvar').data('retorno'); }, 1500);
                },
                error: function (resposta) { 
					var erros = '';
					var retorno = resposta.responseJSON.errors;
					$.each(retorno, function(){
						erros = erros+this+"\n";
					});
                    swal("Erro!", erros, "error");
                },
                complete: function(){
                    //btn_salvar.stop();
                }
            });
		});
	</script>
@endsection