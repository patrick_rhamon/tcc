@extends('template.template', ['titulo' => 'Alterar Curso', 'titulo_pagina' => 'Processo Seletivo'])

@section('css')
@endsection

@section('conteudo')
	<div class="conteudo">
        <section class="au-breadcrumb m-t-75">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span"></span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="{{ route('vaga.listar') }}">Vagas</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Nova Vaga</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="m-t-15">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <form action="{{ route('vaga.alterar-salvar') }}" method="post" id="form" class="form-horizontal">
									<input type="hidden" name="id" id="id" value="{{ $vaga->id }}">
                                    <div class="card-header">
                                        <strong>Nova Vaga</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="processo_id" class=" form-control-label">Processo Seletivo*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select class="form-control" name="processo_id" id="processo_id"  @if(!count($processos)) disabled @endif>
                                                    @if (count($processos)>0)
                                                        <option value="">Selecione um processo seletivo</option>
                                                    @endif
                                                    @forelse ($processos as $processo)
                                                        <option value="{{$processo->id}}"  {{ $vaga->processo_id == $processo->id ? 'selected' : '' }}>{{$processo->str_nome}}</option>
                                                    @empty
                                                        <option value="-1">Nenhum processo seletivo cadastrado</option>
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="unidade_id" class=" form-control-label">Unidade*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select class="form-control" name="unidade_id" id="unidade_id"  @if(!count($unidades)) disabled @endif>
                                                    @if (count($unidades)>0)
                                                        <option value="">Selecione uma unidade</option>
                                                    @endif
                                                    @forelse ($unidades as $unidade)
                                                        <option value="{{$unidade->id}}" {{ $vaga->unidade_id == $unidade->id ? 'selected' : '' }}>{{$unidade->str_nome}}</option>
                                                    @empty
                                                        <option value="-1">Nenhuma unidade cadastrada</option>
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="curso_id" class=" form-control-label">Curso*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select class="form-control" name="curso_id" id="curso_id"  @if(!count($cursos)) disabled @endif>
                                                    @if (count($cursos)>0)
                                                        <option value="">Selecione um curso</option>
                                                    @endif
                                                    @forelse ($cursos as $curso)
                                                        <option value="{{$curso->id}}" {{ $vaga->curso_id == $curso->id ? 'selected' : '' }}>{{$curso->str_nome}}</option>
                                                    @empty
                                                        <option value="-1">Nenhum curso cadastrado</option>
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="int_quantidade" class=" form-control-label">Quantidade de Vagas*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control numerico" required id="int_quantidade" name="int_quantidade" value="{{ $vaga->int_quantidade }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        <button type="button" class="btn btn-danger btn-sm" id="cancelar" data-link="{{ route('vaga.listar') }}">
                                            <i class="fa fa-ban"></i> Cancelar
                                        </button>
                                        <button type="button" class="btn btn-primary ladda-button btn-sm" id="salvar" data-retorno="{{ route('vaga.listar') }}">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
	<script>
		$('#salvar').on('click', function(e){
			e.preventDefault();
			//var btn_salvar = Ladda.create(this);
			//btn_salvar.start();

            if(!$("#form").parsley().validate()){
                //btn_salvar.stop();
                return;
            }
        
            var form = $('#form');

            $.ajax({
                method: $(form).attr("method"),
                url: $(form).attr("action"),
                data: $(form).serializeArray(),
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (retorno) {
					swal("Sucesso!", retorno.mensagem, "success");
                    setTimeout(function() { window.document.location = $('#salvar').data('retorno'); }, 1500);
                },
                error: function (resposta) { 
					var erros = '';
					var retorno = resposta.responseJSON.errors;
					$.each(retorno, function(){
						erros = erros+this+"\n";
					});
                    swal("Erro!", erros, "error");
                },
                complete: function(){
                    //btn_salvar.stop();
                }
            });
		});
	</script>
@endsection