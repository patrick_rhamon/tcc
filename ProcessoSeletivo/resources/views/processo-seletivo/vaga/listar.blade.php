@extends('template.template', ['titulo' => 'Listar Vagas', 'titulo_pagina' => 'Processo Seletivo'])
@section('css')
@endsection

@section('conteudo')
    <div class="conteudo">
        <section class="au-breadcrumb m-t-75">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                        <a href="{{ route('home') }}">Home</a>
                                    </li>
                                    <li class="list-inline-item seprate">
                                        <span>/</span>
                                    </li>
                                    <li class="list-inline-item">Vagas</li>
                                </ul>
                            </div>
                            <button data-link="{{ route('vaga.novo') }}" class="au-btn au-btn-icon au-btn--green au-btn--small" id="novo">
                                <i class="zmdi zmdi-plus"></i>Nova Vaga</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="m-t-15">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- DATA TABLE -->
                            <h3 class="title-5 m-b-35">Listar Vagas</h3>
                            <!-- Pesquisa -->
                            @if(count($vagas))
                                <div class="container-fluid">
                                    <div class="header-wrap">
                                        <form class="form-header" action="{{ route('vaga.listar') }}" method="GET">
                                            <input class="au-input au-input--xl" type="text" name="pesquisa" value="{{ $pesquisa ?? '' }}" placeholder="Informe o Nome do Curso, do Processo ou da Unidade">
                                            <button class="au-btn--submit" type="submit">
                                                <i class="zmdi zmdi-search"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            @endif
                            <!-- Pesquisa -->
                            <div class="table-responsive table-responsive-data2">
                                @if(count($vagas))
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th style="width: 22%;">Processo</th>
                                                <th style="width: 22%;">Unidade</th>
                                                <th style="width: 22%;">Curso</th>
                                                <th class="text-center" style="width: 14%;">Quantidade</th>
                                                <th class="text-center">Ações</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($vagas as $index => $vaga)
                                                @if($index > 0)
                                                <tr class="spacer"></tr>
                                                @endif
                                                <tr class="tr-shadow">
                                                    <td>{{ $vaga->processo->str_nome }}</td>
                                                    <td>{{ $vaga->unidade->str_nome }}</td>
                                                    <td>{{ $vaga->curso->str_nome }}</td>
                                                    <td class="text-center">{{ $vaga->int_quantidade }}</td>
                                                    <td>
                                                        <div class="table-data-feature">
                                                            <button class="item provas" data-toggle="tooltip" data-placement="top" title="Provas" data-link="{{ route('prova.listar', [$vaga->id]) }}">
                                                                <i class="fas fa-file-powerpoint"></i>
                                                            </button>
                                                            <button class="item alterar" data-toggle="tooltip" data-placement="top" title="Alterar" data-link="{{ route('vaga.alterar', [$vaga->id]) }}">
                                                                <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                            <button class="item remover" data-toggle="tooltip" data-placement="top" title="Remover" data-link="{{ route('vaga.remover') }}" data-id="{{ $vaga->id }}">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {!! $vagas->appends(['pesquisa' => $pesquisa])->render() !!} 
                                @else
                                    <div class="alert alert-danger" role="alert">
                                        Não existe vaga cadastrada!
                                    </div>
                                @endif
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script>
        $(".provas").on('click', function(){
            document.location = $(this).data('link');
        });
    </script>
@endsection
