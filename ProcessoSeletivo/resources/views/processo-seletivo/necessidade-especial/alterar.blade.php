@extends('template.template', ['titulo' => 'Alterar Necessidade Especial', 'titulo_pagina' => 'Processo Seletivo'])

@section('css')
    <link href="{{ asset('css/icheck/skins/flat/green.css') }}" rel="stylesheet">
    <link href="{{ asset('css/processo-seletivo/necessidade-especial/style.css') }}" rel="stylesheet">
@endsection

@section('conteudo')
	<div class="x_panel">
		<div class='x_content'>
			<form id='form' action="{{ route('necessidadeEspecial.alterar-salvar') }}"  method="POST" class="form-horizontal form-label-left" novalidate>
				<label class="control-label"> 
                    Campos com asterísco(*) são de preenchimento obrigatório:
                </label>	
				{{ csrf_field() }}
                <input type="hidden" name="id" id="id" value="{{$necessidade_especial->id}}">
				<div class="form-group">
                    <hr/>
					<h5>Informações da Necessidade Especial</h5>
					
					<div class='item input-group col-md-12 col-sm-12 col-xs-12'>
						<label class="control-label col-md-3 col-sm-2 col-xs-12" for='str_nome'>Nome*:</label>
						<div class="col-md-7 col-sm-7 col-xs-12">
							<input type="text" class="form-control upper" name='str_nome' id='str_nome' value="@if(old('str_nome')){{ old('str_nome') }}@else {{ $necessidade_especial->str_nome }} @endif" required>
						</div>
					</div>

                    <div class='item input-group col-md-12 col-sm-12 col-xs-12'>
                        <div class="control-label col-md-3 col-sm-2 col-xs-1 ">
                            <input data-switchery="true" type="checkbox" class="flat" name='bool_acessibilidade' id='bool_acessibilidade' @if($necessidade_especial->bool_acessibilidade) checked @endif>
                        </div>
                        <label class="col-md-7 col-sm-7 col-xs-12 checkbox alinhar-cadeira-quebrada" style="align: left;" for='bool_acessibilidade'>Necessita de Acessibilidade</label>
                    </div>
				</div>
				<div class="clearfix"></div>      
				<div class="form-group centralizar pull-right">
					<a href="{{ route('necessidadeEspecial.listar') }}" class="btn btn-danger load"><i class="mdi mdi-cancel"></i> Cancelar</a>
					<button class="btn btn-success salvar load" retorno="{{ route('necessidadeEspecial.listar') }}"><i class='mdi mdi-check'></i> Salvar</button>
				</div>
			</form>
		</div> 
	</div>
@endsection

@section('scripts') 
    <script src="{{ asset('js/components/typeahead/typeahead.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/processo-seletivo/necessidade-especial/scripts.js') }}" type="text/javascript"></script>
@endsection