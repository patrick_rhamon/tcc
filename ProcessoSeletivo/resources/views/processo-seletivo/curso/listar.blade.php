@extends('template.template', ['titulo' => 'Listar Cursos', 'titulo_pagina' => 'Processo Seletivo'])
@section('css')
@endsection

@section('conteudo')
    <div class="conteudo">
        <section class="au-breadcrumb m-t-75">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                        <a href="{{ route('home') }}">Home</a>
                                    </li>
                                    <li class="list-inline-item seprate">
                                        <span>/</span>
                                    </li>
                                    <li class="list-inline-item">Cursos</li>
                                </ul>
                            </div>
                            <button data-link="{{ route('curso.novo') }}" class="au-btn au-btn-icon au-btn--green au-btn--small" id="novo">
                                <i class="zmdi zmdi-plus"></i>Nova Curso</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="m-t-15">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- DATA TABLE -->
                            <h3 class="title-5 m-b-35">Listar Cursos</h3>
                            <!-- Pesquisa -->
                            @if(count($cursos))
                                <div class="container-fluid">
                                    <div class="header-wrap">
                                        <form class="form-header" action="{{ route('curso.listar') }}" method="GET">
                                            <input class="au-input au-input--xl" type="text" name="pesquisa" value="{{ $pesquisa ?? '' }}" placeholder="Informe o Nome do Curso">
                                            <button class="au-btn--submit" type="submit">
                                                <i class="zmdi zmdi-search"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            @endif
                            <!-- Pesquisa -->
                            <div class="table-responsive table-responsive-data2">
                                @if(count($cursos))
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th style="width: 80%;">Nome</th>
                                                <th class="text-center">Ações</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cursos as $index => $curso)
                                                @if($index > 0)
                                                <tr class="spacer"></tr>
                                                @endif
                                                <tr class="tr-shadow">
                                                    <td>{{ $curso->str_nome }}</td>
                                                    <td>
                                                        <div class="table-data-feature">
                                                            <button class="item alterar" data-toggle="tooltip" data-placement="top" title="Alterar" data-link="{{ route('curso.alterar', [$curso->id]) }}">
                                                                <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                            <button class="item remover" data-toggle="tooltip" data-placement="top" title="Remover" data-link="{{ route('curso.remover') }}" data-id="{{ $curso->id }}">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {!! $cursos->appends(['pesquisa' => $pesquisa])->render() !!} 
                                @else
                                    <div class="alert alert-danger" role="alert">
                                        Não existe curso cadastrado!
                                    </div>
                                @endif
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
@endsection
