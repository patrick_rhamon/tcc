@extends('template.template', ['titulo' => 'Novo Curso', 'titulo_pagina' => 'Processo Seletivo'])

@section('css')
@endsection

@section('conteudo')
    <div class="conteudo">
        <section class="au-breadcrumb m-t-75">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span"></span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="{{ route('curso.listar') }}">Cursos</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Novo Curso</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="m-t-15">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <form action="{{ route('curso.novo-salvar') }}" method="post" id="form" class="form-horizontal">
                                    <div class="card-header">
                                        <strong>Novo Curso</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="str_nome" class=" form-control-label">Nome*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="str_nome" required name="str_nome" placeholder="Nome do Curso" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="str_area_ensino" class=" form-control-label">Área de Ensino</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="str_area_ensino" name="str_area_ensino" placeholder="Área de Ensino do Curso" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        <button type="button" class="btn btn-danger btn-sm" id="cancelar" data-link="{{ route('curso.listar') }}">
                                            <i class="fa fa-ban"></i> Cancelar
                                        </button>
                                        <button type="button" class="btn btn-primary ladda-button btn-sm" id="salvar" data-retorno="{{ route('curso.listar') }}">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script>
        $('#salvar').on('click', function(e){
            e.preventDefault();
            //var btn_salvar = Ladda.create(this);
            //btn_salvar.start();

            if(!$("#form").parsley().validate()){
                //btn_salvar.stop();
                return;
            }
        
            var form = $('#form');

            $.ajax({
                method: $(form).attr("method"),
                url: $(form).attr("action"),
                data: $(form).serializeArray(),
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (retorno) {
                    swal("Sucesso!", retorno.mensagem, "success");
                    setTimeout(function() { window.document.location = $('#salvar').data('retorno'); }, 1500);
                },
                error: function (resposta) { 
                    var erros = '';
                    var retorno = resposta.responseJSON.errors;
                    $.each(retorno, function(){
                        erros = erros+this+"\n";
                    });
                    swal("Erro!", erros, "error");
                },
                complete: function(){
                    //btn_salvar.stop();
                }
            });
        });
    </script>
@endsection