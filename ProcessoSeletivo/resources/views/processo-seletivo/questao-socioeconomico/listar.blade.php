@extends('template.template', ['titulo' => 'Listar Questões Socioeconomicos', 'titulo_pagina' => 'Processo Seletivo'])
@section('css')  
    <link href="{{ asset('css/processo-seletivo/questao-socioeconomico/style.css') }}" rel="stylesheet"> 
    <link href="{{ asset('css/icheck/skins/flat/green.css') }}" rel="stylesheet">
@endsection

@section('conteudo') 
    <div class="x_panel">
        <div class="x_title">Informe questão socioeconomico que deseja Pesquisar </div>
        <div class="x_content">
            <form action = "{{ route('questaoSocioeconomico.listar', '$questionario_socioeconomico->id') }}" method="GET" class="form-horizontal form-label-left"  >
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-10 col-sm-10 col-xs-12 col-sm-offset-1 col-md-offset-1">

                        <div class="form-group">
                            <div class='input-group col-md-12 col-sm-12 col-xs-12'>
                                <label class="control-label col-md-2 col-sm-1 col-xs-12" for='str_pesquisa'>Busca:</label>
                            
                                <div class="col-md-10 col-sm-11 col-xs-12">
                                    <input type="text" class="form-control upper" name='str_pesquisa' value='@if(isset($pesquisa)) {{ $pesquisa }} @endif'>
                                </div>
                            </div>
                        </div>

                        <div class="form-group centralizar">
                            <button type="submit" id='pesquisa_btn' class="btn btn-primary busca"><i class="mdi mdi-magnify"></i> Buscar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">Questões Socioeconomicos encontrados na busca:</div>
        <div class="x_content">
            {!! $questoes_socioeconomicos->appends(['str_pesquisa' => $pesquisa])->render() !!}  
            <table class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" id="questionarioSocioeconomico-datatable" role="grid">
                <colgroup>
                    <col span="1" style="width: 5%;">
                    <col span="1" style="width: 95%;">
                </colgroup>
                <thead>
                    <tr role="row">
                        <th class="centralizar" rowspan="1" colspan="1">#</th>
                        <th class="" rowspan="1" colspan="1">Nome</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($questoes_socioeconomicos as $questao_socioeconomico)
                        <tr>
                            <td class='centralizar'>
                                <div class="iradio_flat-green" style="position: relative;">
                                    <input type="radio" class="flat" name="item">
                                    <form action="{{ route('questaoSocioeconomico.remover', $questionario_socioeconomico->id) }}" method='post' >
                                        <input type='hidden' value='{{ $questao_socioeconomico->id }}' name='id'>
                                    </form>
                                </div>
                            </td>
                            <td>{{$questao_socioeconomico->str_pergunta }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan='2'>Não foram encontrados registros</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <br>
            <div class="form-group centralizar">
                <a href="{{ route('questaoSocioeconomico.novo', $questionario_socioeconomico->id) }}" class="btn btn-primary load"><i class="mdi mdi-plus"></i> Novo</a>
                <button class="btn btn-info alterar load" action="{{ route('questaoSocioeconomico.alterar', [$questionario_socioeconomico->id, '']) }}" disabled><i class="mdi mdi-pencil"></i> Alterar</button>
                <button class="btn btn-danger remover load" disabled><i class="mdi mdi-delete-empty"></i> Remover</button>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/components/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/components/jquery.mask.js') }}"></script>
    <script src="{{ asset('js/components/datatables/jquery.dataTables.min.js') }}" ></script>
    <script src="{{ asset('js/components/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/processo-seletivo/questao-socioeconomico/scripts.js') }}" type="text/javascript"></script>
@endsection
