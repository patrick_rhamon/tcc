@extends('template.template', ['titulo' => 'Nova Questão Socioeconomico', 'titulo_pagina' => 'Processo Seletivo'])

@section('css')
    <link href="{{ asset('css/icheck/skins/flat/green.css') }}" rel="stylesheet">
    <link href="{{ asset('css/processo-seletivo/questao-socioeconomico/style.css') }}" rel="stylesheet">
@endsection

@section('conteudo')
	<div class="x_panel">
		<div class='x_content'>
			<form id='form' action="{{ route('questaoSocioeconomico.novo-salvar', [$questionario_socioeconomico->id]) }}"  method="POST" class="form-horizontal form-label-left" novalidate>
				<label class="control-label"> 
                    Campos com asterísco(*) são de preenchimento obrigatório:
                </label>	
				{{ csrf_field() }}
				<div class="form-group">
                    <hr/>
					<h5>Informações da Questão Socioeconomico</h5>
					<input type="hidden" name="questionario_id" value="{{ $questionario_socioeconomico->id }}">

					<div class='item input-group col-md-12 col-sm-12 col-xs-12'>
						<label class="control-label col-md-3 col-sm-2 col-xs-12" for='str_pergunta'>Pergunta*:</label>
						<div class="col-md-7 col-sm-7 col-xs-12">
							<input type="text" class="form-control upper" name='str_pergunta' id='str_pergunta' value="{{ old('str_pergunta') }}" required>
						</div>
					</div>

					<div class='item input-group col-md-12 col-sm-12 col-xs-12'>
						<label class="control-label col-md-3 col-sm-2 col-xs-12" for='str_pergunta'>Tipo de Resposta*:</label>
						<div class="col-md-7 col-sm-7 col-xs-12">
							<select class="form-control" name="int_tipo_resposta" id="int_tipo_resposta" @if(!count($filtros)) disabled @endif>
								@if(count($filtros)) <option value="">Selecione uma opção</option> @endif
								@forelse($filtros as $filtro)
									<option value="{{ $filtro['id'] }}">{{ $filtro['descricao'] }}</option>
								@empty
									<option value="">Nenhuma opção informada</option>
								@endforelse
							</select>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>      
				<div class="form-group centralizar pull-right">
					<a href="{{ route('questaoSocioeconomico.listar', [$questionario_socioeconomico->id]) }}" class="btn btn-danger load"><i class="mdi mdi-cancel"></i> Cancelar</a>
					<button class="btn btn-success salvar load" retorno="{{ route('questaoSocioeconomico.listar', [$questionario_socioeconomico->id]) }}"><i class='mdi mdi-check'></i> Salvar</button>
				</div>
			</form>
		</div> 
	</div>
@endsection

@section('scripts') 
    <script src="{{ asset('js/components/typeahead/typeahead.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/processo-seletivo/questao-socioeconomico/scripts.js') }}" type="text/javascript"></script>
@endsection