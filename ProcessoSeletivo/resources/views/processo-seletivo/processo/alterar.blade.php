@extends('template.template', ['titulo' => 'Alterar Processo Seletivo', 'titulo_pagina' => 'Processo Seletivo'])

@section('css')
@endsection

@section('conteudo')
    <div class="conteudo">
        <section class="au-breadcrumb m-t-75">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span"></span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="{{ route('processo.listar') }}">Processos Seletivos</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Alterar Processo Seletivo</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="m-t-15">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <form action="{{ route('processo.alterar-salvar') }}" method="post" id="form" class="form-horizontal">
                                    <input type="hidden" name="id" id="id" value="{{ $processo->id }}">
                                    <div class="card-header">
                                        <strong>Novo Processo Seletivo</strong>
                                    </div>
                                    <div class="card-body card-block">
                                        <hr class="m-t-0 m-b-10" />    
                                        <h5>Informações do Processo Seletivo</h5>
                                        <hr class="m-t-10 m-b-15" />

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="str_nome" class=" form-control-label">Nome*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" id="str_nome" required name="str_nome" placeholder="Nome do Processo Seletivo" value="{{ $processo->str_nome }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="data_inicio" class=" form-control-label">Data de Início*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control data" id="data_inicio" name="data_inicio" value="{{ date('d/m/Y', strtotime($processo->data_inicio)) }}" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="data_fim" class=" form-control-label">Data de Término*</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control data" id="data_fim" name="data_fim" value="{{ date('d/m/Y', strtotime($processo->data_fim)) }}" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="questionario_socioeconomico_id" class=" form-control-label">Questionário Socioeconômico</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select class="form-control" name="questionario_socioeconomico_id" id="questionario_socioeconomico_id" @if(!count($questionarios)) disabled @endif>
                                                    @if(count($questionarios)>0)
                                                        <option value="">Selecione um questionário socioeconômico</option>
                                                    @endif
                                                    @forelse($questionarios as $questionario)
                                                        <option value="{{$questionario->id}}" {{ $processo->questionario_socioeconomico_id == $questionario->id ? 'selected' : '' }}>{{$questionario->str_nome}}</option>
                                                    @empty
                                                        <option value="-1">Nenhuma questionário cadastrado</option>
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>

                                        <hr class="m-t-15 m-b-10"/>
                                        <h5>Sobre o pagamento do Processo Seletivo</h5>
                                        <hr class="m-t-10 m-b-15" />

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="data_recebimento" class=" form-control-label">Data de Recebimento</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control data" id="data_recebimento" name="data_recebimento" value="{{ date('d/m/Y', strtotime($processo->data_recebimento)) }}">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="float_valor" class=" form-control-label">Valor</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control monetario" id="float_valor" name="float_valor" value="{{ $processo->float_valor }}">
                                            </div>
                                        </div>

                                        <hr class="m-t-15 m-b-10"/>
                                        <h5>Sobre a Prova do Processo Seletivo</h5>
                                        <hr class="m-t-10 m-b-15" />

                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="data_prova" class=" form-control-label">Data da Prova</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control data" id="data_prova" name="data_prova" value="{{ date('d/m/Y', strtotime($processo->data_prova)) }}">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="data_prova" class=" form-control-label">Turno</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select class="form-control upper" name="str_turno" id="str_turno">
                                                    <option value="">Selecione um Turno</option>
                                                    <option value="MATUTINO" {{ $processo->str_turno == 'MATUTINO' ? 'selected' : '' }}>Matutino</option>
                                                    <option value="VESPERTINO" {{ $processo->str_turno == 'VESPERTINO' ? 'selected' : '' }}>Vespertino</option>
                                                    <option value="NOTURNO" {{ $processo->str_turno == 'NOTURNO' ? 'selected' : '' }}>Noturno</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="data_prova" class=" form-control-label">Tipo do Processo</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select class="form-control upper" name="str_tipo" id="str_tipo">
                                                    <option value="">Selecione um Tipo de Processo</option>
                                                    <option value="SIMPLES" {{ $processo->str_tipo == 'SIMPLES' ? 'selected' : '' }}>Simples</option>
                                                    <option value="REMANESCENTE" {{ $processo->str_tipo == 'REMANESCENTE' ? 'selected' : '' }}>Remanescente</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        <button type="button" class="btn btn-danger btn-sm" id="cancelar" data-link="{{ route('processo.listar') }}">
                                            <i class="fa fa-ban"></i> Cancelar
                                        </button>
                                        <button type="button" class="btn btn-primary ladda-button btn-sm" id="salvar" data-retorno="{{ route('processo.listar') }}">
                                            <i class="fa fa-save"></i> Salvar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script>
        $('#salvar').on('click', function(e) {
            e.preventDefault();
            //var btn_salvar = Ladda.create(this);
            //btn_salvar.start();

            if(!$("#form").parsley().validate()){
                //btn_salvar.stop();
                return;
            }
        
            var form = $('#form');

            $.ajax({
                method: $(form).attr("method"),
                url: $(form).attr("action"),
                data: $(form).serializeArray(),
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (retorno) {
                    swal("Sucesso!", retorno.mensagem, "success");
                    setTimeout(function() { window.document.location = $('#salvar').data('retorno'); }, 1500);
                },
                error: function (resposta) { 
                    var erros = '';
                    var retorno = resposta.responseJSON.errors;
                    $.each(retorno, function(){
                        erros = erros+this+"\n";
                    });
                    swal("Erro!", erros, "error");
                },
                complete: function(){
                    //btn_salvar.stop();
                }
            });
        });
    </script>
@endsection