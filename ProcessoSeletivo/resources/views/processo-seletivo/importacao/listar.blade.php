@extends('template.template', ['titulo' => 'Listar Importações', 'titulo_pagina' => 'Processo Seletivo'])
@section('css')
@endsection

@section('conteudo')
    <div class="conteudo">
        <section class="au-breadcrumb m-t-75">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                        <a href="{{ route('home') }}">Home</a>
                                    </li>
                                    <li class="list-inline-item seprate">
                                        <span>/</span>
                                    </li>
                                    <li class="list-inline-item">Importações</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="m-t-15">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                        <div class="col-md-12">
                            <!-- DATA TABLE -->
                            <h3 class="title-5 m-b-35">Listar Importações</h3>
                            <!-- Pesquisa -->
                            <div class="container-fluid m-b-15">
                                <div class="header-wrap">
                                    <form action="{{ route('importacao.enviar.arquivo') }}" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="row">
                                            <div class="custom-file col-11">
                                                <input type="file" class="custom-file-input" required id="arquivo" name="arquivo" accept=".txt">
                                                <label class="custom-file-label" for="customFile">Escolha o arquivo</label>
                                            </div>
                                            <div class="col-1">
                                                <button type="submit" class="btn btn-primary ladda-button btn-sm" id="enviar">
                                                    <i class="fas fa-arrow-circle-right"></i> Enviar
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @if(count($importacoes))
                                <div class="container-fluid">
                                    <div class="header-wrap">
                                        <form class="form-header" action="{{ route('importacao.listar') }}" method="GET">
                                            <input class="au-input au-input--xl" type="text" name="pesquisa" value="{{ $pesquisa ?? '' }}" placeholder="Informe o Nome do Arquivo">
                                            <button class="au-btn--submit" type="submit">
                                                <i class="zmdi zmdi-search"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            @endif
                            <!-- Pesquisa -->
                            <div class="table-responsive table-responsive-data2">
                                @if(count($importacoes))
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th style="width: 40%;">Nome</th>
                                                <th style="width: 30%;">Status</th>
                                                <th style="width: 30%;">Data Processamento</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($importacoes as $index => $importacao)
                                                @if($index > 0)
                                                <tr class="spacer"></tr>
                                                @endif
                                                <tr class="tr-shadow">
                                                    <td>{{ $importacao->str_nome_original }}</td>
                                                    <td>{{ $statuses[$importacao->int_status] }}</td>
                                                    <td>{{ $importacao->data_processamento ? date('d/m/Y H:i:s', strtotime($importacao->data_processamento)) : '' }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {!! $importacoes->appends(['pesquisa' => $pesquisa])->render() !!} 
                                @else
                                    <div class="alert alert-danger" role="alert">
                                        Não existe arquivos de importações!
                                    </div>
                                @endif
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script>
        $(".provas").on('click', function(){
            document.location = $(this).data('link');
        });
    </script>
@endsection
